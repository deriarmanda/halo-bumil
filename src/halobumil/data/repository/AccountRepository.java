/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.repository;

import halobumil.assets.string.AppMessage;
import halobumil.data.model.Akun;
import halobumil.util.DatabaseHelper;
import halobumil.util.LoggerUtils;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author DERI
 */
public class AccountRepository {
    
    private final String TAG_NAME = AccountRepository.class.getSimpleName();
    private final String QUERY_LOGIN = 
            "SELECT * FROM akun WHERE username = ? AND password = ?;";
    private final String QUERY_REGISTER = 
            "INSERT INTO akun(username, password) VALUES(?, ?);";
    private final String QUERY_UPDATE = 
            "UPDATE akun SET username = ?, password = ? WHERE uid = ?;";
    
    private static AccountRepository sInstance;
    private final DatabaseHelper dbHelper;
    private Akun currentAkun;
    
    private AccountRepository() {
        dbHelper = DatabaseHelper.getInstance();
    }
    
    public static AccountRepository getInstance() {
        if (sInstance == null) sInstance = new AccountRepository();
        return sInstance;
    }
    
    public String checkAccountInDatabase(String username, String password) {
        try {
            ResultSet rs = dbHelper.readData(QUERY_LOGIN, username, password);
            if (rs.next()) {
                currentAkun = new Akun(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3)
                );
                return AppMessage.LOGIN_SUCCESS;
            } else {
                currentAkun = null;
                return AppMessage.LOGIN_FAILED;
            }
        } catch(SQLException ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
            return AppMessage.ERROR_GENERAL_DATABASE;
        }
    }
    
    public String createNewAccount(String username, String password, boolean activeUser) {
        try {
            int insertedId = dbHelper.getInsertedIdQuery(
                    QUERY_REGISTER, 
                    username, 
                    password
            );
            
            if (activeUser) {
                currentAkun = new Akun(
                        insertedId,
                        username,
                        password
                );
            }
            return AppMessage.REGISTER_SUCCESS;
        } catch (SQLException ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
            return AppMessage.ERROR_GENERAL_DATABASE;
        }
    }
    
    public String updateCurrentAccount(String username, String password) {
        try {
            if (currentAkun == null) throw new SQLException(AppMessage.ERROR_INACTIVE_ACCOUNT);
            dbHelper.executeQuery(QUERY_UPDATE, username, password, currentAkun.getUid());
            currentAkun.setUsername(username);
            currentAkun.setPassword(password);
            
            return AppMessage.UPDATE_ACCOUNT_SUCCESS;
        } catch (SQLException ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
            return AppMessage.ERROR_GENERAL_DATABASE;
        }
    }

    public Akun getCurrentAkun() {
        return currentAkun;
    }

    public void setCurrentAkun(Akun currentAkun) {
        this.currentAkun = currentAkun;
    }
    
}
