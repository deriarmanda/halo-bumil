/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.repository;

import halobumil.assets.string.AppMessage;
import halobumil.data.model.Trimester;
import halobumil.util.DatabaseHelper;
import halobumil.util.LoggerUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DERI
 */
public class TrimesterRepository {
    
    private final String TAG_NAME = TrimesterRepository.class.getSimpleName();
    private final String QUERY_READ_TRIMESTER = 
            "SELECT * FROM trimester WHERE uid_pasien = ? AND tipe = ?;";
    private final String QUERY_INSERT_TRIMESTER = 
            "INSERT INTO trimester(uid_pasien, tanggal, petugas, tipe) "
            + "VALUES (?, ?, ?, ?);";
    private final String QUERY_DELETE_TRIMESTER = 
            "DELETE FROM trimester WHERE uid = ? AND tipe = ?;";
    
    private static TrimesterRepository sInstance;
    private final ObservableList<Trimester> listTrimester1;
    private final ObservableList<Trimester> listTrimester2;
    private final ObservableList<Trimester> listTrimester3;
    private final DatabaseHelper dbHelper;
    
    private TrimesterRepository() {
        listTrimester1 = FXCollections.observableArrayList();
        listTrimester2 = FXCollections.observableArrayList();
        listTrimester3 = FXCollections.observableArrayList();
        dbHelper = DatabaseHelper.getInstance();
    }
    
    public static TrimesterRepository getInstance() {
        if (sInstance == null) sInstance = new TrimesterRepository();
        return sInstance;
    }
    
    public ObservableList<Trimester> getListTrimester1() {
        return listTrimester1;
    }
    
    public ObservableList<Trimester> getListTrimester2() {
        return listTrimester2;
    }
    
    public ObservableList<Trimester> getListTrimester3() {
        return listTrimester3;
    }
    
    public void loadListTrimester1(Integer uidPasien, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    listTrimester1.clear();
                    ResultSet rs = dbHelper.readData(
                            QUERY_READ_TRIMESTER, 
                            uidPasien, 1
                    );
                    while (rs.next()) {
                        Trimester t = new Trimester(
                                rs.getInt(1),
                                rs.getInt(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getInt(5)
                        );
                        listTrimester1.add(t);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onLoadListTrimester1Succeed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }

    public void insertTrimester1(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    int insertedId = dbHelper.getInsertedIdQuery(
                            QUERY_INSERT_TRIMESTER, 
                            t.getUidPasien().getValue(),
                            t.getTanggal().get(),
                            t.getPetugas().get(), 1
                    );
                    t.setUid(insertedId);
                    listTrimester1.add(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            //callback.onLoadListTrimester3Succeed(listTrimester1);
                            callback.onInsertTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void deleteTrimester1(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Integer uid = t.getUid().get();
                    if (uid == -1) throw new SQLException(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
                    dbHelper.executeQuery(
                            QUERY_DELETE_TRIMESTER, 
                            uid, 1
                    );
                    listTrimester1.remove(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onDeleteTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public void loadListTrimester2(Integer uidPasien, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    listTrimester2.clear();
                    ResultSet rs = dbHelper.readData(
                            QUERY_READ_TRIMESTER, 
                            uidPasien, 2
                    );
                    while (rs.next()) {
                        Trimester t = new Trimester(
                                rs.getInt(1),
                                rs.getInt(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getInt(5)
                        );
                        listTrimester2.add(t);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onLoadListTrimester2Succeed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void insertTrimester2(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    int insertedId = dbHelper.getInsertedIdQuery(
                            QUERY_INSERT_TRIMESTER,
                            t.getUidPasien().getValue(),
                            t.getTanggal().get(),
                            t.getPetugas().get(), 2
                    );
                    t.setUid(insertedId);
                    listTrimester2.add(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            //callback.onLoadListTrimester3Succeed(listTrimester2);
                            callback.onInsertTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void deleteTrimester2(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Integer uid = t.getUid().get();
                    if (uid == -1) throw new SQLException(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
                    dbHelper.executeQuery(
                            QUERY_DELETE_TRIMESTER, 
                            uid, 2
                    );
                    listTrimester2.remove(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onDeleteTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public void loadListTrimester3(Integer uidPasien, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    listTrimester3.clear();
                    ResultSet rs = dbHelper.readData(
                            QUERY_READ_TRIMESTER, 
                            uidPasien, 3
                    );
                    while (rs.next()) {
                        Trimester t = new Trimester(
                                rs.getInt(1),
                                rs.getInt(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getInt(5)
                        );
                        listTrimester3.add(t);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onLoadListTrimester3Succeed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void insertTrimester3(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    int insertedId = dbHelper.getInsertedIdQuery(
                            QUERY_INSERT_TRIMESTER,
                            t.getUidPasien().getValue(),
                            t.getTanggal().get(),
                            t.getPetugas().get(), 3
                    );
                    t.setUid(insertedId);
                    listTrimester3.add(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            //callback.onLoadListTrimester3Succeed(listTrimester3);
                            callback.onInsertTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void deleteTrimester3(Trimester t, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Integer uid = t.getUid().get();
                    if (uid == -1) throw new SQLException(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
                    dbHelper.executeQuery(
                            QUERY_DELETE_TRIMESTER, 
                            uid, 3
                    );
                    listTrimester3.remove(t);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onDeleteTrimesterSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public interface Callback {
        void onLoadListTrimester1Succeed();
        void onLoadListTrimester2Succeed();
        void onLoadListTrimester3Succeed();
        void onInsertTrimesterSucceed();
        void onDeleteTrimesterSucceed();
        void onRepositoryTaskFailed(String msg);
    }
    
}
