/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.repository;

import halobumil.assets.string.AppMessage;
import halobumil.data.model.SMS;
import halobumil.util.DatabaseHelper;
import halobumil.util.DateTimeUtils;
import halobumil.util.DialogBuilder;
import halobumil.util.LoggerUtils;
import halobumil.util.SmsSenderThread;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsmsengine.CMessage;
import org.jsmsengine.COutgoingMessage;
import org.jsmsengine.CService;

/**
 *
 * @author DERI
 */
public class SMSRepository {
    
    private final String TAG_NAME = SMSRepository.class.getSimpleName();
    private final String QUERY_READ_DATA_SMS = 
            "SELECT * FROM pesan;";
    private final String QUERY_UPDATE_SMS = 
            "UPDATE pesan SET teks_kemarin = ?, "
            + "teks_pagi = ?, teks_sore = ?, com_port = ? WHERE uid = 1;";
    private final String QUERY_UPDATE_STATUS_SMS = 
            "UPDATE trimester SET status_pesan_kemarin = ?, "
            + "status_pesan_pagi = ?, status_pesan_sore = ? "
            + "WHERE uid = ?;";
    private final String QUERY_GET_TODAY_LIST = 
            "SELECT t.uid, p.nama, p.telepon, t.tanggal, t.status_pesan_kemarin, "
            + "t.status_pesan_pagi, t.status_pesan_sore FROM pasien p "
            + "INNER JOIN trimester t ON p.uid = t.uid_pasien WHERE "
            + "(t.tanggal = ? AND t.status_pesan_sore = ?) OR"
            + "(t.tanggal = ? AND t.status_pesan_kemarin = ?);";
    
    private static SMSRepository sInstance;
    private final DatabaseHelper dbHelper;
    private final DialogBuilder dialogBuilder;
    private final SmsSenderThread sender;
    private CService modem;
    
    private String teksKemarin, teksPagi, teksSore, comPort;
    
    private SMSRepository() {
        dbHelper = DatabaseHelper.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        sender = SmsSenderThread.getInstance();
        
        teksKemarin = "";
        teksPagi = "";
        teksSore = "";
        loadDataSMS();
    }
    
    public static SMSRepository getInstance() {
        if (sInstance == null) sInstance = new SMSRepository();
        return sInstance;
    }
    
    public void updateDataSMS(
            String kemarin, String pagi, 
            String sore, String port, Callback callback
    ) {
        new Thread() {
            @Override
            public void run() {
                try {
                    dbHelper.executeQuery(
                            QUERY_UPDATE_SMS, 
                            kemarin, pagi, sore, port
                    );
                    teksKemarin = kemarin;
                    teksPagi = pagi;
                    teksSore = sore;
                    comPort = port;
                    
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onUpdateSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void sendSms(ObservableList<SMS> list, Callback callback) {
        new Thread() {
            @Override
            public void run() {
//                if (!modem.getConnected()) {
//                    try {
//                        modem.connect();
//                        Platform.runLater(() -> {
//                            callback.onModemConnected(
//                                    "Berhasil terhubung dengan Modem GSM dengan nama : "
//                                            +modem.getDeviceInfo().getManufacturer()
//                            );
//                        });
//                    } catch (Exception ex) {
//                        LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
//                        Platform.runLater(() -> {
//                            callback.onRepositoryTaskFailed(
//                                "Terjadi kesalahan saat menghubungkan ke Modem GSM.\n"
//                                        + "Pastikan Modem GSM telah terhubung dengan "
//                                        + "menggunakan Port yang telah diatur di menu Pengaturan "
//                                        + "dan restart aplikasi."
//                            );
//                        });
//                        return;
//                    }
//                }
                
                for (SMS sms : list) {
                    String humanDate = sms.getHumanFormatTanggal().get(),
                            number = sms.getTelepon().get();
                    if (humanDate.equals("Besok")) {
                        if (sms.getStatusKemarin().get().equals("Menunggu")) {
//                            COutgoingMessage msgBuilder = new COutgoingMessage(
//                                    number, teksKemarin
//                            );
//                            msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);

                            try {
//                                modem.sendMessage(msgBuilder);
                                sender.sendSms(number, teksKemarin);
                                sms.setStatusKemarin("Terkirim");
                                
                            } catch (Exception ex) {
                                LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                                Platform.runLater(() -> {
                                    callback.onRepositoryTaskFailed(
                                            "Terjadi kesalahan ketika mengirim SMS ke nomor "
                                                    +number+".\n("+ex.getMessage()+")."
                                    );
                                });
                                return;
                            }
                        }
                    } else {
                        boolean isMorning = DateTimeUtils.isMorningNow();
                        if (isMorning) {
                            if (sms.getStatusPagi().get().equals("Menunggu")) {
//                                COutgoingMessage msgBuilder = new COutgoingMessage(
//                                        number, teksPagi
//                                );
//                                msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);

                                try {
//                                    modem.sendMessage(msgBuilder);
                                    sender.sendSms(number, teksPagi);
                                    sms.setStatusKemarin("Terkirim");
                                    sms.setStatusPagi("Terkirim");
                                } catch (Exception ex) {
                                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                                    Platform.runLater(() -> {
                                        callback.onRepositoryTaskFailed(
                                                "Terjadi kesalahan ketika mengirim SMS ke nomor "
                                                        +number+".\n("+ex.getMessage()+")."
                                        );
                                    });
                                    return;
                                }
                            }
                        } else {
                            if (sms.getStatusSore().get().equals("Menunggu")) {
//                                COutgoingMessage msgBuilder = new COutgoingMessage(
//                                        number, teksSore
//                                );
//                                msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);

                                try {
//                                    modem.sendMessage(msgBuilder);
                                    sender.sendSms(number, teksSore);
                                    sms.setStatusKemarin("Terkirim");
                                    sms.setStatusPagi("Terkirim");
                                    sms.setStatusSore("Terkirim");
                                } catch (Exception ex) {
                                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                                    Platform.runLater(() -> {
                                        callback.onRepositoryTaskFailed(
                                                "Terjadi kesalahan ketika mengirim SMS ke nomor "
                                                        +number+".\n("+ex.getMessage()+")."
                                        );
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    try {
                        dbHelper.executeQuery(
                                QUERY_UPDATE_STATUS_SMS,
                                sms.getStatusKemarin().get(),
                                sms.getStatusPagi().get(),
                                sms.getStatusSore().get(),
                                sms.getUid()
                        );
                    } catch (SQLException ex) {
                        LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                        return;
                    }
                }
                Platform.runLater(() -> {
                    callback.onSendSmsSucceed();
                });
            }        
        }.start();
    }
    
    public void sendSms(String number, String msg, Callback callback) {
        new Thread() {
            @Override
            public void run() { 
//                if (!modem.getConnected()) {
//                    try {
//                        modem.connect();
//                        System.out.println("modem connected.");
//                        Platform.runLater(() -> {
//                            callback.onModemConnected(
//                                    "Berhasil terhubung dengan Modem GSM dengan nama : "
//                                            +modem.getDeviceInfo().getManufacturer()
//                            );
//                        });
//                    } catch (Exception ex) {
//                        LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
//                        ex.printStackTrace();
//                        Platform.runLater(() -> {
//                            callback.onRepositoryTaskFailed(
//                                "Terjadi kesalahan saat menghubungkan ke Modem GSM.\n"
//                                        + "Pastikan Modem GSM telah terhubung dengan "
//                                        + "menggunakan Port yang telah diatur di menu Pengaturan "
//                                        + "dan restart aplikasi."
//                            );
//                        });
//                        return;
//                    }
//                }
//                
//                COutgoingMessage msgBuilder = new COutgoingMessage(
//                        number, msg
//                );
//                msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);

                try {
//                    modem.sendMessage(msgBuilder);
                    sender.sendSms(number, msg);
                    System.out.println("message sent.");
                } catch (Exception ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                    ex.printStackTrace();
                    Platform.runLater(() -> {
                        callback.onRepositoryTaskFailed(
                                "Terjadi kesalahan ketika mengirim SMS ke nomor "
                                        +number+".\n("+ex.getMessage()+")."
                        );
                    });
                    return;
                }
                Platform.runLater(() -> {
                    callback.onSendSmsSucceed();
                });
            }        
        }.start();
    }
    
    public void getTodaySMSList(Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    ObservableList<SMS> list = FXCollections.observableArrayList();
                    ResultSet rs = dbHelper.readData(
                            QUERY_GET_TODAY_LIST,
                            DateTimeUtils.format(LocalDate.now()),
                            "Menunggu",
                            DateTimeUtils.format(LocalDate.now().plusDays(1)),
                            "Menunggu"
                    );
                    while (rs.next()) {
                        SMS t = new SMS(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7)
                        );
                        list.add(t);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onGetTodaySMSListSucceed(list);
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public void updateStatusSMS(SMS sms, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    dbHelper.executeQuery(
                            QUERY_UPDATE_STATUS_SMS,
                            sms.getStatusKemarin().get(),
                            sms.getStatusPagi().get(),
                            sms.getStatusSore().get(),
                            sms.getUid()
                    );
                    Platform.runLater(() -> {
                        callback.onUpdateSmsSucceed();
                    });
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    Platform.runLater(() -> {
                        callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                    });
                }
            }
        }.start();
    }
    
    private void loadDataSMS() {
        new Thread() {
            @Override
            public void run() {
                try {
                    ResultSet rs = dbHelper.readData(QUERY_READ_DATA_SMS);
                    if (rs.next()) {
                        teksKemarin = rs.getString(2);
                        teksPagi = rs.getString(3);
                        teksSore = rs.getString(4);
                        comPort = rs.getString(5);
//                        modem = new CService(comPort, 9999, "Huawei", ""); 
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    dialogBuilder.showErrorDialog(AppMessage.ERROR_GENERAL_DATABASE);
                }
            }
        }.start();
    }
    
    private void sendSms(String number, String msg) {
        COutgoingMessage msgBuilder = new COutgoingMessage(
                number, msg
        );
        msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);

        try {
//            modem.sendMessage(msgBuilder);
            sender.sendSms(number, msg);
        } catch (Exception ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
            dialogBuilder.showErrorDialog(
                    "Terjadi kesalahan ketika mengirim SMS ke nomor "
                            +number+".\n("+ex.getMessage()+")."
            );
        }
    }

    public String getTeksKemarin() {
        return teksKemarin;
    }

    public String getTeksPagi() {
        return teksPagi;
    }

    public String getTeksSore() {
        return teksSore;
    }

    public String getComPort() {
        return comPort;
    }
    
    public interface Callback {
        void onUpdateSucceed();
        void onGetTodaySMSListSucceed(ObservableList<SMS> list);
        void onModemConnected(String msg);
        void onSendSmsSucceed();
        void onUpdateSmsSucceed();
        void onRepositoryTaskFailed(String msg);
    }
}
