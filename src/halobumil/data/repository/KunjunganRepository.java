/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.repository;

import halobumil.assets.string.AppMessage;
import halobumil.data.model.KunjunganPasien;
import halobumil.data.model.Laporan;
import halobumil.util.DatabaseHelper;
import halobumil.util.DateTimeUtils;
import halobumil.util.LoggerUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DERI
 */
public class KunjunganRepository {
    
    private final String TAG_NAME = KunjunganRepository.class.getSimpleName();
    private final String QUERY_READ_TODAY_LIST = 
            "SELECT * FROM kunjungan WHERE waktu LIKE ?;";
    private final String QUERY_READ_NO_RM = 
            "SELECT no_rm FROM pasien WHERE uid = ?;";
    private final String QUERY_INSERT_KUNJUNGAN = 
            "INSERT INTO kunjungan(uid_pasien, nama, alamat, waktu) "
            + "VALUES (?, ?, ?, ?);";
    private final String QUERY_UPDATE_UID_PASIEN = 
            "UPDATE kunjungan SET uid_pasien = ? WHERE uid = ?;";
    private final String QUERY_DELETE_KUNJUNGAN = 
            "DELETE FROM kunjungan WHERE uid = ?;";
    private final String QUERY_KUNJUNGAN_TEPAT = 
            "SELECT p.no_rm, p.nama, substr(k.waktu, 0, 11) as tgl_kunjungan, t.tanggal as tgl_trimester "
            + "FROM pasien p INNER JOIN kunjungan k INNER JOIN trimester t "
            + "ON p.uid = k.uid_pasien AND k.uid_pasien = t.uid_pasien "
            + "WHERE tgl_kunjungan = tgl_trimester "
            + "AND substr(tgl_kunjungan, 7, 4) = ? ";
    private final String QUERY_KUNJUNGAN_TIDAK_TEPAT = 
            "SELECT p.no_rm, p.nama, substr(k.waktu, 0, 11) as tgl_kunjungan, t.tanggal as tgl_trimester "
            + "FROM pasien p INNER JOIN kunjungan k INNER JOIN trimester t "
            + "ON p.uid = k.uid_pasien AND k.uid_pasien = t.uid_pasien "
            + "WHERE tgl_kunjungan != tgl_trimester "
            + "AND substr(tgl_kunjungan, 4, 2) = substr(tgl_trimester, 4, 2) "
            + "AND substr(tgl_kunjungan, 7, 4) = substr(tgl_trimester, 7, 4) "
            + "AND substr(tgl_kunjungan, 7, 4) = ? ";
    private final String QUERY_KUNJUNGAN_MONTHLY = 
            "AND substr(tgl_kunjungan, 4, 2) = ? ;";
    
    private static KunjunganRepository sINSTANCE;
    private final ObservableList<KunjunganPasien> listKunjungan;
    private final ObservableList<KunjunganPasien> waitingListKunjungan;
    private final DatabaseHelper dbHelper;
    
    private KunjunganRepository() {
        listKunjungan = FXCollections.observableArrayList();
        waitingListKunjungan = FXCollections.observableArrayList();
        dbHelper = DatabaseHelper.getInstance();
    }
    
    public static KunjunganRepository getInstance() {
        if (sINSTANCE == null) sINSTANCE = new KunjunganRepository();
        return sINSTANCE;
    }

    public void reloadListKunjungan(Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    listKunjungan.clear();
                    ResultSet rs = dbHelper.readData(
                            QUERY_READ_TODAY_LIST, 
                            DateTimeUtils.format(LocalDate.now())+"%"
                    );
                    while (rs.next()) {
                        int uid = rs.getInt(1), uidPasien = rs.getInt(2);
                        String noRM = "", 
                                nama = rs.getString(3), 
                                alamat = rs.getString(4), 
                                tanggal = rs.getString(5);
                        
                        if (rs.getInt(2) != -1) {
                            ResultSet rsP = dbHelper.readData(
                                    QUERY_READ_NO_RM, 
                                    rs.getInt(2)
                            );
                            if (rsP.next()) noRM = rsP.getString(1);
                        }
                        KunjunganPasien kp = new KunjunganPasien(
                                uid,
                                uidPasien,
                                noRM,
                                nama,
                                alamat,
                                tanggal
                        );
                        listKunjungan.add(kp);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onLoadListKunjunganSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public ObservableList<KunjunganPasien> getListKunjungan() {
        return listKunjungan;
    }
    
    public ObservableList<KunjunganPasien> getWaitingListKunjungan() {
        waitingListKunjungan.clear();
        waitingListKunjungan.addAll(listKunjungan.filtered(
                kunjungan -> !kunjungan.isStatusActive())
        );
        return waitingListKunjungan;
    }
    
    public void getKunjunganTepatWaktu(String bulan, String tahun, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    ObservableList<Laporan> list = FXCollections.observableArrayList();
                    ResultSet rs;
                    if (bulan.equals("ALL")) {
                        rs = dbHelper.readData(
                                QUERY_KUNJUNGAN_TEPAT + ";",
                                tahun
                        );
                    } else {
                        rs = dbHelper.readData(QUERY_KUNJUNGAN_TEPAT + QUERY_KUNJUNGAN_MONTHLY,
                            tahun, bulan
                        );
                    }
                    while (rs.next()) {
                        Laporan laporan = new Laporan(
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4)
                        );
                        list.add(laporan);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onGetKunjunganTepatWaktuSucceed(list);
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void getKunjunganTidakTepat(String bulan, String tahun, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    ObservableList<Laporan> list = FXCollections.observableArrayList();
                    ResultSet rs;
                    if (bulan.equals("ALL")) {
                        rs = dbHelper.readData(
                                QUERY_KUNJUNGAN_TIDAK_TEPAT + ";",
                                tahun
                        );
                    } else {
                        rs = dbHelper.readData(QUERY_KUNJUNGAN_TIDAK_TEPAT + QUERY_KUNJUNGAN_MONTHLY,
                            tahun, bulan
                        );
                    }
                    while (rs.next()) {
                        Laporan laporan = new Laporan(
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4)
                        );
                        list.add(laporan);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onGetKunjunganTidakTepatSucceed(list);
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void getListKunjunganKemarin(Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    ObservableList<KunjunganPasien> list = FXCollections.observableArrayList();
                    ResultSet rs = dbHelper.readData(
                            QUERY_READ_TODAY_LIST, 
                            DateTimeUtils.format(LocalDate.now().minusDays(1))+"%"
                    );
                    while (rs.next()) {
                        int uid = rs.getInt(1), uidPasien = rs.getInt(2);
                        String noRM = "", 
                                nama = rs.getString(3), 
                                alamat = rs.getString(4), 
                                tanggal = rs.getString(5);
                        
                        if (rs.getInt(2) != -1) {
                            ResultSet rsP = dbHelper.readData(
                                    QUERY_READ_NO_RM, 
                                    rs.getInt(2)
                            );
                            if (rsP.next()) noRM = rsP.getString(1);
                        }
                        KunjunganPasien kp = new KunjunganPasien(
                                uid,
                                uidPasien,
                                noRM,
                                nama,
                                alamat,
                                tanggal
                        );
                        list.add(kp);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onGetListKunjunganKemarinSucceed(list);
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public void insertNewKunjungan(KunjunganPasien kunjungan, Callback callback) {
        new InsertKunjunganThread(kunjungan, callback).start();
    }
    
    public void updateInsertedPasien(Integer uid, Integer uidPasien, Callback callback) throws SQLException {
        dbHelper.executeQuery(QUERY_UPDATE_UID_PASIEN, uidPasien, uid);
        reloadListKunjungan(callback);
    }
    
    public void deleteKunjungan(KunjunganPasien kunjungan, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    dbHelper.executeQuery(
                            QUERY_DELETE_KUNJUNGAN, 
                            kunjungan.getUid().get()
                    );
                    listKunjungan.remove(kunjungan);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onDeleteKunjunganSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    private class InsertKunjunganThread extends Thread {

        KunjunganPasien kunjungan;
        Callback callback;

        public InsertKunjunganThread(KunjunganPasien kunjungan, Callback callback) {
            this.kunjungan = kunjungan;
            this.callback = callback;
        }
                
        @Override
        public void run() {
            try {
                int insertedId = dbHelper.getInsertedIdQuery(
                        QUERY_INSERT_KUNJUNGAN, 
                        kunjungan.getUidPasien().getValue(),
                        kunjungan.getNama().get(),
                        kunjungan.getAlamat().get(),
                        kunjungan.getTanggal().get()
                );
                kunjungan.setUid(insertedId);
                listKunjungan.add(kunjungan);  
                Platform.runLater(() -> {
                    callback.onInsertNewKunjunganSucceed();
                });     
            } catch (SQLException ex) {
                LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                Platform.runLater(() -> {
                    callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                });
            }     
        }
    }
    
    public interface Callback {
        void onLoadListKunjunganSucceed();
        void onInsertNewKunjunganSucceed();
        void onDeleteKunjunganSucceed();
        void onGetKunjunganTepatWaktuSucceed(ObservableList<Laporan> list);
        void onGetKunjunganTidakTepatSucceed(ObservableList<Laporan> list);
        void onGetListKunjunganKemarinSucceed(ObservableList<KunjunganPasien> list);
        void onRepositoryTaskFailed(String msg);
    }
}
