/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.repository;

import halobumil.assets.string.AppMessage;
import halobumil.data.model.Pasien;
import halobumil.util.DatabaseHelper;
import halobumil.util.LoggerUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DERI
 */
public class PasienRepository {
    
    private final String TAG_NAME = PasienRepository.class.getSimpleName();
    private final String QUERY_READ_ALL_LIST = 
            "SELECT * from pasien;";
    private final String QUERY_INSERT_PASIEN = 
            "INSERT INTO pasien(no_rm, nama, alamat, tanggal_lahir, "
            + "telepon, g, p, a, haid, hpht, hpl, bb, mual, pusing, nyeri, "
            + "gerakJanin, oedema, nafsuMakan, pendarahan, penyakit, "
            + "riwayatPenyakit, kebiasaanIbu, statusTT, hivAids) VALUES ("
            + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
            + "?, ?, ?, ?, ?, ?);";
    private final String QUERY_UPDATE_PASIEN = 
            "UPDATE pasien SET no_rm = ?, nama = ?, alamat = ?, tanggal_lahir = ?, "
            + "telepon = ?, g = ?, p = ?, a = ?, haid = ?, hpht = ?, "
            + "hpl = ?, bb = ?, mual = ?, pusing = ?, nyeri = ?, "
            + "gerakJanin = ?, oedema = ?, nafsuMakan = ?, pendarahan = ?, penyakit = ?, "
            + "riwayatPenyakit = ?, kebiasaanIbu = ?, statusTT = ?, hivAids = ? WHERE uid = ?;";
    private final String QUERY_DELETE_PASIEN =
            "DELETE FROM pasien WHERE uid = ?;";
    
    private static PasienRepository sInstance;
    private final ObservableList<Pasien> listPasien;
    private final DatabaseHelper dbHelper;
    private final KunjunganRepository kunjunganRepo;
    
    private PasienRepository() {
        listPasien = FXCollections.observableArrayList();
        dbHelper = DatabaseHelper.getInstance();
        kunjunganRepo = KunjunganRepository.getInstance();
    }
    
    public static PasienRepository getInstance() {
        if (sInstance == null) sInstance = new PasienRepository();
        return sInstance;
    }
    
    public void reloadListPasien(Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    listPasien.clear();
                    ResultSet rs = dbHelper.readData(QUERY_READ_ALL_LIST);
                    while (rs.next()) {
                        if (rs.getInt(1) == -1) continue;
                        Pasien p = new Pasien(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8),
                                rs.getString(9),
                                rs.getString(10),
                                rs.getString(11),
                                rs.getString(12),
                                rs.getString(13),
                                rs.getString(14),
                                rs.getString(15),
                                rs.getString(16),
                                rs.getString(17),
                                rs.getString(18),
                                rs.getString(19),
                                rs.getString(20),
                                rs.getString(21),
                                rs.getString(22),
                                rs.getString(23),
                                rs.getString(24),
                                rs.getString(25)
                        );
                        listPasien.add(p);
                    }
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onLoadListPasienSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public ObservableList<Pasien> getListPasien() {
        return listPasien;
    }
    
    public void insertAndUpdatePasienBaru(
            int uidKunjungan, 
            Pasien pasien, 
            Callback callback,
            KunjunganRepository.Callback kunjunganCallback
    ) {
        new Thread() {
            @Override
            public void run() {
                try {
                    int insertedId = dbHelper.getInsertedIdQuery(
                        QUERY_INSERT_PASIEN, 
                        pasien.getNoRM().get(),
                        pasien.getNama().get(),
                        pasien.getAlamat().get(),
                        pasien.getTanggalLahir().get(),
                        pasien.getTelepon().get(),
                        pasien.getDataG().get(),
                        pasien.getDataP().get(),
                        pasien.getDataA().get(),
                        pasien.getHaid().get(),
                        pasien.getHpht().get(),
                        pasien.getHpl().get(),
                        pasien.getBb().get(),
                        pasien.getMual().get(),
                        pasien.getPusing().get(),
                        pasien.getNyeri().get(),
                        pasien.getGerakJanin().get(),
                        pasien.getOedema().get(),
                        pasien.getNafsuMakan().get(),
                        pasien.getPendarahan().get(),
                        pasien.getPenyakit().get(),
                        pasien.getRiwayatPenyakit().get(),
                        pasien.getKebiasaanIbu().get(),
                        pasien.getStatusTT().get(),
                        pasien.getHivAids().get()
                    );
                    pasien.setUid(insertedId);
                    if (uidKunjungan >= 0)
                        kunjunganRepo.updateInsertedPasien(uidKunjungan, insertedId, kunjunganCallback);
                    listPasien.add(pasien);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onInsertPasienSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public void deletePasien(Pasien pasien, Callback callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Integer uid = pasien.getUid().get();
                    if (uid == -1) throw new SQLException(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
                    dbHelper.executeQuery(
                            QUERY_DELETE_PASIEN, 
                            uid
                    );
                    listPasien.remove(pasien);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onDeletePasienSucceed();
                        });
                    }
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }            
        }.start();
    }
    
    public void updatePasien(
            Pasien pasien, 
            String noRM, 
            String nama,
            String alamat, 
            String tglLahir, 
            String telepon, 
            String dataG, 
            String dataP, 
            String dataA, 
            String haid, 
            String hpht, 
            String hpl, 
            String bb, 
            String mual, 
            String pusing, 
            String nyeri, 
            String gerakJanin, 
            String oedema, 
            String nafsuMakan, 
            String pendarahan, 
            String penyakit, 
            String riwayatPenyakit, 
            String kebiasaanIbu, 
            String statusTT,
            String hivAids,
            Callback callback
    ) {
        new Thread() {
            @Override
            public void run() {
                try {                    
                    listPasien.remove(pasien);
                    dbHelper.executeQuery(
                            QUERY_UPDATE_PASIEN, 
                            noRM, 
                            nama, 
                            alamat, 
                            tglLahir, 
                            telepon, 
                            dataG, 
                            dataP, 
                            dataA, 
                            haid, 
                            hpht, 
                            hpl, 
                            bb, 
                            mual, 
                            pusing, 
                            nyeri, 
                            gerakJanin, 
                            oedema, 
                            nafsuMakan, 
                            pendarahan, 
                            penyakit, 
                            riwayatPenyakit, 
                            kebiasaanIbu, 
                            statusTT,
                            hivAids,
                            pasien.getUid().get()
                    );
                    pasien.setNoRM(noRM); 
                    pasien.setNama(nama);
                    pasien.setAlamat(alamat); 
                    pasien.setTanggalLahir(tglLahir);
                    pasien.setTelepon(telepon);
                    
                    pasien.setDataG(dataG);
                    pasien.setDataP(dataP);
                    pasien.setDataA(dataA);
                    pasien.setHaid(haid);
                    pasien.setHpht(hpht);
                    pasien.setHpl(hpl);
                    pasien.setBb(bb);
                    pasien.setMual(mual);
                    pasien.setPusing(pusing);
                    pasien.setNyeri(nyeri);
                    pasien.setGerakJanin(gerakJanin);
                    pasien.setOedema(oedema);
                    pasien.setNafsuMakan(nafsuMakan);
                    pasien.setPendarahan(pendarahan);
                    pasien.setPenyakit(penyakit);
                    pasien.setRiwayatPenyakit(riwayatPenyakit);
                    pasien.setKebiasaanIbu(kebiasaanIbu);
                    pasien.setStatusTT(statusTT);
                    pasien.setHivAids(hivAids);
                    
                    listPasien.add(pasien);
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onUpdatePasienSucceed(pasien);
                        });
                    }
                } catch (SQLException ex) {
                    listPasien.add(pasien);
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
                    if (callback != null) {
                        Platform.runLater(() -> {
                            callback.onRepositoryTaskFailed(AppMessage.ERROR_GENERAL_DATABASE);
                        });
                    }
                }
            }
        }.start();
    }
    
    public interface Callback {
        void onLoadListPasienSucceed();
        void onInsertPasienSucceed();
        void onDeletePasienSucceed();
        void onUpdatePasienSucceed(Pasien pasien);
        void onRepositoryTaskFailed(String msg);
    } 
}
