/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.model;

import halobumil.util.DateTimeUtils;
import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
public class SMS {
    
    private final Integer uid;
    private final StringProperty nama;
    private final StringProperty telepon;
    private final StringProperty tanggal;
    private final StringProperty statusKemarin;
    private final StringProperty statusPagi;
    private final StringProperty statusSore;
    private final StringProperty humanFormatTanggal;
    private final StringProperty currentStatus;
    
    public SMS(Integer uid, String nama, String telepon, String tanggal, String statusKemarin, String statusPagi, String statusSore) {
        this.uid = uid;
        this.nama = new SimpleStringProperty(nama);
        this.telepon = new SimpleStringProperty(telepon);
        this.tanggal = new SimpleStringProperty(tanggal);
        this.statusKemarin = new SimpleStringProperty(statusKemarin);
        this.statusPagi = new SimpleStringProperty(statusPagi);
        this.statusSore = new SimpleStringProperty(statusSore);
        
        this.humanFormatTanggal = new SimpleStringProperty(
                tanggal.compareTo(
                    DateTimeUtils.format(LocalDate.now())
                ) > 0? "Besok" : "Hari Ini "+(DateTimeUtils.isMorningNow()? "(Pagi)":"(Sore)")
        );
        if (this.humanFormatTanggal.get().equals("Besok")) {
            this.currentStatus = new SimpleStringProperty(statusKemarin);
        } else if (this.humanFormatTanggal.get().contains("Pagi")) {
            this.currentStatus = new SimpleStringProperty(statusPagi);
        } else {
            this.currentStatus = new SimpleStringProperty(statusSore);
        }
    }

    public Integer getUid() {
        return uid;
    }

    public StringProperty getNama() {
        return nama;
    }
    
    public void setNama(String nama) {
        this.nama.set(nama);
    }

    public StringProperty getTelepon() {
        return telepon;
    }
    
    public void setTelepon(String telepon) {
        this.telepon.set(telepon);
    }

    public StringProperty getTanggal() {
        return tanggal;
    }
    
    public void setTanggal(String tanggal) {
        this.tanggal.set(tanggal);
        this.humanFormatTanggal.set(tanggal.compareTo(
                    DateTimeUtils.format(LocalDate.now())
                ) > 0? "Besok" : "Hari Ini"
        );
    }

    public StringProperty getStatusKemarin() {
        return statusKemarin;
    }
    
    public void setStatusKemarin(String statusKemarin) {
        this.statusKemarin.set(statusKemarin);
        this.currentStatus.set(
                statusKemarin.equals("Menunggu")? statusKemarin : (
                        statusPagi.get().equals("Menunggu")? statusPagi.get() : statusSore.get()
                )
        );
    }

    public StringProperty getStatusPagi() {
        return statusPagi;
    }
    
    public void setStatusPagi(String statusPagi) {
        this.statusPagi.set(statusPagi);
        this.currentStatus.set(
                statusKemarin.get().equals("Menunggu")? statusKemarin.get() : (
                        statusPagi.equals("Menunggu")? statusPagi : statusSore.get()
                )
        );
    }

    public StringProperty getStatusSore() {
        return statusSore;
    }
    
    public void setStatusSore(String statusSore) {
        this.statusSore.set(statusSore);
        this.currentStatus.set(
                statusKemarin.get().equals("Menunggu")? statusKemarin.get() : (
                        statusPagi.get().equals("Menunggu")? statusPagi.get() : statusSore
                )
        );
    }
    
    public StringProperty getHumanFormatTanggal() {
        return humanFormatTanggal;
    }

    public StringProperty getCurrentStatus() {
        return currentStatus;
    }
    
}
