/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.model;

import halobumil.util.DateTimeUtils;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
public class KunjunganPasien {
    
    private final IntegerProperty uid;
    private final IntegerProperty uidPasien;
    private final StringProperty noRM;
    private final StringProperty nama;
    private final StringProperty alamat;
    private final StringProperty tanggal;
    private final StringProperty waktu;
    private final StringProperty status;

    public KunjunganPasien(Integer uid, Integer uidPasien, String noRM, String nama, String alamat, String tanggal) {
        this.uid = new SimpleIntegerProperty(uid);
        this.uidPasien = new SimpleIntegerProperty(uidPasien);
        this.noRM = new SimpleStringProperty(noRM);
        this.nama = new SimpleStringProperty(nama);
        this.alamat = new SimpleStringProperty(alamat);
        this.tanggal = new SimpleStringProperty(tanggal);
        
        waktu = new SimpleStringProperty(DateTimeUtils.getTimeOnly(tanggal));
        status = new SimpleStringProperty(
                (uidPasien == -1? "Menunggu" : "Aktif")
        );
    }

    public IntegerProperty getUid() {
        return uid;
    }
    
    public void setUid(int uid) {
        this.uid.set(uid);
    }

    public IntegerProperty getUidPasien() {
        return uidPasien;
    }
    
    public void setUidPasien(int uidPasien) {
        this.uidPasien.set(uidPasien);
        status.set((uidPasien == -1? "Menunggu" : "Aktif"));
    }

    public StringProperty getNoRM() {
        return noRM;
    }

    public void setNoRM(String noRM) {
        this.noRM.set(noRM);
    }
    
    public StringProperty getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama.set(nama);
    }
    
    public StringProperty getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat.set(alamat);
    }
    
    public StringProperty getTanggal() {
        return tanggal;
    }
    
    public StringProperty getWaktu() {
        return waktu;
    }
    
    public void setTanggal(String tanggal) {
        this.tanggal.set(tanggal);
        waktu.set(DateTimeUtils.getTimeOnly(tanggal));
    }

    public StringProperty getStatus() {
        return status;
    }

    public boolean isStatusActive() {
        return status.get().equals("Aktif");
    }

    @Override
    public String toString() {
        return uid.get() + ", " + uidPasien.get() + ", " 
                + nama.get() + ", " + noRM.get() + ", " 
                + alamat.get() + ", " + tanggal.get() + ", " + status.get();
    }
    
}
