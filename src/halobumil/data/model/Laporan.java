/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
public class Laporan {
    
    private final StringProperty noRM;
    private final StringProperty nama;
    private final StringProperty tglKunjungan;
    private final StringProperty tglTrimester;
    
    public Laporan(String noRM, String nama, String tglKunjungan, String tglTrimester) {
        this.noRM = new SimpleStringProperty(noRM);
        this.nama = new SimpleStringProperty(nama);
        this.tglKunjungan = new SimpleStringProperty(tglKunjungan);
        this.tglTrimester = new SimpleStringProperty(tglTrimester);
    }

    public StringProperty getNoRM() {
        return noRM;
    }
    
    public void setNoRM(String noRM) {
        this.noRM.set(noRM);
    }

    public StringProperty getNama() {
        return nama;
    }
    
    public void setNama(String nama) {
        this.nama.set(nama);
    }

    public StringProperty getTglKunjungan() {
        return tglKunjungan;
    }
    
    public void setTglKunjungan(String tglKunjungan) {
        this.tglKunjungan.set(tglKunjungan);
    }

    public StringProperty getTglTrimester() {
        return tglTrimester;
    }
    
    public void setTglTrimester(String tglTrimester) {
        this.tglTrimester.set(tglTrimester);
    }    
    
}
