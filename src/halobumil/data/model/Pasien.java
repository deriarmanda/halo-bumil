/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
public class Pasien {
    
    private final IntegerProperty uid;
    private final StringProperty noRM;
    private final StringProperty nama;
    private final StringProperty alamat;
    private final StringProperty tanggalLahir;
    private final StringProperty telepon;
    
    private final StringProperty dataG;
    private final StringProperty dataP;
    private final StringProperty dataA;
    private final StringProperty haid;
    private final StringProperty hpht;
    private final StringProperty hpl;
    private final StringProperty bb;
    private final StringProperty mual;
    private final StringProperty pusing;
    private final StringProperty nyeri;
    private final StringProperty gerakJanin;
    private final StringProperty oedema;
    private final StringProperty nafsuMakan;
    private final StringProperty pendarahan;
    private final StringProperty penyakit;
    private final StringProperty riwayatPenyakit;
    private final StringProperty kebiasaanIbu;
    private final StringProperty statusTT;
    private final StringProperty hivAids;
    
    public Pasien(
            Integer uid, 
            String noRM, 
            String nama, 
            String alamat, 
            String tanggalLahir, 
            String telepon, 
            String dataG, 
            String dataP, 
            String dataA, 
            String haid, 
            String hpht, 
            String hpl, 
            String bb, 
            String mual, 
            String pusing, 
            String nyeri, 
            String gerakJanin, 
            String oedema, 
            String nafsuMakan, 
            String pendarahan, 
            String penyakit,
            String riwayatPenyakit,
            String kebiasaanIbu,
            String statusTT,
            String hivAids
    ) {
        this.uid = new SimpleIntegerProperty(uid);
        this.noRM = new SimpleStringProperty(noRM);
        this.nama = new SimpleStringProperty(nama);
        this.alamat = new SimpleStringProperty(alamat);
        this.tanggalLahir = new SimpleStringProperty(tanggalLahir);
        this.telepon = new SimpleStringProperty(telepon);
        
        this.dataG = new SimpleStringProperty(dataG);
        this.dataP = new SimpleStringProperty(dataP);
        this.dataA = new SimpleStringProperty(dataA);
        this.haid = new SimpleStringProperty(haid);
        this.hpht = new SimpleStringProperty(hpht);
        this.hpl = new SimpleStringProperty(hpl);
        this.bb = new SimpleStringProperty(bb);
        this.mual = new SimpleStringProperty(mual);
        this.pusing = new SimpleStringProperty(pusing);
        this.nyeri = new SimpleStringProperty(nyeri);
        this.gerakJanin = new SimpleStringProperty(gerakJanin);
        this.oedema = new SimpleStringProperty(oedema);
        this.nafsuMakan = new SimpleStringProperty(nafsuMakan);
        this.pendarahan = new SimpleStringProperty(pendarahan);
        this.penyakit = new SimpleStringProperty(penyakit);
        this.riwayatPenyakit = new SimpleStringProperty(riwayatPenyakit);
        this.kebiasaanIbu = new SimpleStringProperty(kebiasaanIbu);
        this.statusTT = new SimpleStringProperty(statusTT);
        this.hivAids = new SimpleStringProperty(hivAids);
    }
    

    public IntegerProperty getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid.set(uid);
    }
    
    public StringProperty getNoRM() {
        return noRM;
    }

    public void setNoRM(String noRM) {
        this.noRM.set(noRM);
    }
    
    public StringProperty getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama.set(nama);
    }
    
    public StringProperty getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat.set(alamat);
    }
    
    public StringProperty getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir.set(tanggalLahir);
    }
    
    public StringProperty getTelepon() {
        return telepon;
    }
    
    public void setTelepon(String telepon) {
        this.telepon.set(telepon);
    }

    public StringProperty getDataG() {
        return dataG;
    }

    public StringProperty getDataP() {
        return dataP;
    }

    public StringProperty getDataA() {
        return dataA;
    }

    public StringProperty getHaid() {
        return haid;
    }

    public StringProperty getHpht() {
        return hpht;
    }

    public StringProperty getHpl() {
        return hpl;
    }

    public StringProperty getBb() {
        return bb;
    }

    public StringProperty getMual() {
        return mual;
    }

    public StringProperty getPusing() {
        return pusing;
    }

    public StringProperty getNyeri() {
        return nyeri;
    }

    public StringProperty getGerakJanin() {
        return gerakJanin;
    }

    public StringProperty getOedema() {
        return oedema;
    }

    public StringProperty getNafsuMakan() {
        return nafsuMakan;
    }

    public StringProperty getPendarahan() {
        return pendarahan;
    }

    public StringProperty getPenyakit() {
        return penyakit;
    }

    public StringProperty getRiwayatPenyakit() {
        return riwayatPenyakit;
    }

    public StringProperty getKebiasaanIbu() {
        return kebiasaanIbu;
    }

    public StringProperty getStatusTT() {
        return statusTT;
    }

    public StringProperty getHivAids() {
        return hivAids;
    }

    public void setDataG(String dataG) {
        this.dataG.set(dataG);
    }

    public void setDataP(String dataP) {
        this.dataP.set(dataP);
    }

    public void setDataA(String dataA) {
        this.dataA.set(dataA);
    }

    public void setHaid(String haid) {
        this.haid.set(haid);
    }

    public void setHpht(String hpht) {
        this.hpht.set(hpht);
    }

    public void setHpl(String hpl) {
        this.hpl.set(hpl);
    }

    public void setBb(String bb) {
        this.bb.set(bb);
    }

    public void setMual(String mual) {
        this.mual.set(mual);
    }

    public void setPusing(String pusing) {
        this.pusing.set(pusing);
    }

    public void setNyeri(String nyeri) {
        this.nyeri.set(nyeri);
    }

    public void setGerakJanin(String gerakJanin) {
        this.gerakJanin.set(gerakJanin);
    }

    public void setOedema(String oedema) {
        this.oedema.set(oedema);
    }

    public void setNafsuMakan(String nafsuMakan) {
        this.nafsuMakan.set(nafsuMakan);
    }

    public void setPendarahan(String pendarahan) {
        this.pendarahan.set(pendarahan);
    }

    public void setPenyakit(String penyakit) {
        this.penyakit.set(penyakit);
    }

    public void setRiwayatPenyakit(String riwayatPenyakit) {
        this.riwayatPenyakit.set(riwayatPenyakit);
    }

    public void setKebiasaanIbu(String kebiasaanIbu) {
        this.kebiasaanIbu.set(kebiasaanIbu);
    }

    public void setStatusTT(String statusTT) {
        this.statusTT.set(statusTT);
    }

    public void setHivAids(String hivAids) {
        this.hivAids.set(hivAids);
    }
    
}
