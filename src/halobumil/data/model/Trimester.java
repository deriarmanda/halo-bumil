/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.data.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DERI
 */
public class Trimester {
    
    private final IntegerProperty uid;
    private final IntegerProperty uidPasien;
    private final StringProperty tanggal;
    private final StringProperty petugas;
    private final IntegerProperty tipe;

    public Trimester(Integer uid, Integer uidPasien, String tanggal, String petugas, Integer tipe) {
        this.uid = new SimpleIntegerProperty(uid);
        this.uidPasien = new SimpleIntegerProperty(uidPasien);
        this.tanggal = new SimpleStringProperty(tanggal);
        this.petugas = new SimpleStringProperty(petugas);
        this.tipe = new SimpleIntegerProperty(tipe);
    }

    public IntegerProperty getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid.set(uid);
    }

    public IntegerProperty getUidPasien() {
        return uidPasien;
    }

    public void setUidPasien(Integer uidPasien) {
        this.uidPasien.set(uidPasien);
    }

    public StringProperty getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal.set(tanggal);
    }

    public StringProperty getPetugas() {
        return petugas;
    }

    public void setPetugas(String petugas) {
        this.petugas.set(petugas);
    }       

    public IntegerProperty getTipe() {
        return tipe;
    }

    public void setTipe(Integer tipe) {
        this.tipe.set(tipe);
    } 
}
