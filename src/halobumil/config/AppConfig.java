/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.config;

import javafx.scene.image.Image;

/**
 *
 * @author DERI
 */
public class AppConfig {
    
    private static AppConfig sINSTANCE;
    
    public final String APP_NAME;
    public final Image APP_ICON;
    
    private boolean running;
    
    private AppConfig() {
        running = true;
        
        APP_NAME = "Halo Bumil";
        APP_ICON = new Image("halobumil/assets/image/logo_app.png");
    }

    public static AppConfig getInstance() {
        if (sINSTANCE == null) sINSTANCE = new AppConfig();
        return sINSTANCE;
    }
    
    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

}
