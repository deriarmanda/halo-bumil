/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.config;

import halobumil.util.LoggerUtils;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author DERI
 */
public class DatabaseConfig {
    
    private static final String TAG_NAME = DatabaseConfig.class.getSimpleName();
    private static final String DB_NAME = "halo_bumil.db";
    private static final String DB_PATH = 
            System.getProperty("user.dir")+File.separator+DB_NAME;
    private static final String JDBC_CONFIG = "jdbc:sqlite:" + DB_PATH;
    
    private static Connection dbConn;
    
    public static Connection getConnection() {
        if (dbConn == null) try {
            dbConn = DriverManager.getConnection(JDBC_CONFIG);
        } catch (SQLException ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.toString());
            dbConn = null;
        }
        return dbConn;
    }
}
