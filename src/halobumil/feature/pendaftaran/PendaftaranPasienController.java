/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.pendaftaran;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.KunjunganPasien;
import halobumil.data.model.Laporan;
import halobumil.data.model.Pasien;
import halobumil.data.repository.KunjunganRepository;
import halobumil.data.repository.PasienRepository;
import halobumil.util.DateTimeUtils;
import halobumil.util.DialogBuilder;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class PendaftaranPasienController implements Initializable {

    private final String TAG_NAME = 
            PendaftaranPasienController.class.getSimpleName();
    
    // General and Layouting Usage
    @FXML private JFXProgressBar progressBar;
    @FXML private GridPane container;
    
    // Form Kunjungan Pasien Usage
    @FXML private JFXTextField formNoRM;
    @FXML private JFXTextField formNama;
    @FXML private JFXTextField formAlamat;
    @FXML private JFXButton buttonDaftar;
    @FXML private JFXButton buttonBatalkan;
        
    // Table Kunjungan Pasien Usage 
    @FXML private TableView<KunjunganPasien> tableKunjungan;
    @FXML private TableColumn<KunjunganPasien, String> kColumnNoRM;
    @FXML private TableColumn<KunjunganPasien, String> kColumnNama;
    @FXML private TableColumn<KunjunganPasien, String> kColumnAlamat;
    @FXML private TableColumn<KunjunganPasien, String> kColumnStatus;
    @FXML private Label textStatusMsg;
    @FXML private JFXButton buttonUbah;
    @FXML private JFXButton buttonHapus;
    
    // Table Data Pasien Usage
    @FXML private TableView<Pasien> tablePasien;
    @FXML private TableColumn<Pasien, String> pColumnNoRM;
    @FXML private TableColumn<Pasien, String> pColumnNama;
    @FXML private TableColumn<Pasien, String> pColumnAlamat;
    @FXML private TableColumn<Pasien, String> pColumnTglLahir;
    @FXML private TableColumn<Pasien, String> pColumnTelepon;
    @FXML private JFXTextField formSearch;
    
    
    private KunjunganRepository kunjunganRepo;
    private KunjunganPasien selectedKunjunganPasien;
    private KunjunganRepository.Callback kunjunganCallback;
    private PasienRepository pasienRepo;
    private Pasien selectedPasien;
    private PasienRepository.Callback pasienCallback;
    private DialogBuilder dialogBuilder;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kunjunganRepo = KunjunganRepository.getInstance();
        pasienRepo = PasienRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        
        initCallbacks();
        initTableKunjungan();
        initTablePasien();
    }     
    
    public void onPageShown() {
        progressBar.setVisible(true);
        container.setDisable(true);
        kunjunganRepo.reloadListKunjungan(kunjunganCallback);
        pasienRepo.reloadListPasien(pasienCallback);
    }
    
    @FXML
    private void addKunjunganPasien() {
        progressBar.setVisible(true);
        container.setDisable(true);
        String cmd = buttonDaftar.getText();
        
        if (cmd.equals("Daftar")) {
            KunjunganPasien kunjungan = new KunjunganPasien(
                    /*uninserted id*/ -1,
                    -1,
                    formNoRM.getText().trim(),
                    formNama.getText().trim(),
                    formAlamat.getText().trim(),
                    DateTimeUtils.format(LocalDateTime.now())
            );
            kunjunganRepo.insertNewKunjungan(
                    kunjungan, 
                    kunjunganCallback
            );
        } else if (cmd.equals("Simpan") && selectedPasien != null) {
            KunjunganPasien kunjungan = new KunjunganPasien(
                    -1,
                    selectedPasien.getUid().get(),
                    selectedPasien.getNoRM().get(),
                    selectedPasien.getNama().get(),
                    selectedPasien.getAlamat().get(),
                    DateTimeUtils.format(LocalDateTime.now())
            );
            kunjunganRepo.insertNewKunjungan(kunjungan, kunjunganCallback);
        }
    }
    @FXML
    private void cancelFormKunjunganPasien() {
        String cmd = buttonDaftar.getText();
        
        if (cmd.equals("Daftar")) {
            clearForms();
        } else if (cmd.equals("Simpan")) {
            tablePasien.getSelectionModel().clearSelection();
            clearForms();
            disableForms(false);
            buttonDaftar.setText("Daftar");
            selectedPasien = null;
        }
    }
    @FXML
    private void editSelectedKunjunganPasien() { 
        if (selectedKunjunganPasien != null) {
            formNoRM.setText(selectedKunjunganPasien.getNoRM().get());
            formNama.setText(selectedKunjunganPasien.getNama().get());
            formAlamat.setText(selectedKunjunganPasien.getAlamat().get());
            buttonDaftar.setText("Simpan");
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    @FXML
    private void deleteSelectedKunjunganPasien() {
        if (selectedKunjunganPasien != null) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_KUNJUNGAN_CONFIRMATION, () -> {
                        progressBar.setVisible(true);
                        container.setDisable(true);
                        kunjunganRepo.deleteKunjungan(
                                selectedKunjunganPasien, 
                                kunjunganCallback
                        );
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    @FXML
    private void searchPasienByNoRM() {
        String query = formSearch.getText();
        tablePasien.getItems().stream().filter(
                item -> item.getNoRM().get().contains(query)
        ).findAny().ifPresent(item -> {
            tablePasien.requestFocus();
            tablePasien.getSelectionModel().select(item);
            tablePasien.scrollTo(item);
        });
    }
    
    private void initTableKunjungan() {
        kColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        kColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        kColumnAlamat.setCellValueFactory(value -> value.getValue().getAlamat());
        kColumnStatus.setCellValueFactory(value -> value.getValue().getStatus());
        
//        tableKunjungan.setItems(kunjunganRepo.getListKunjungan());
        tableKunjungan.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends KunjunganPasien> observable, 
                KunjunganPasien oldValue, 
                KunjunganPasien newValue) -> {
                    selectedKunjunganPasien = observable.getValue();
                    if (selectedKunjunganPasien != null) {
                        textStatusMsg.setVisible(
                                !selectedKunjunganPasien.isStatusActive()
                        );
                    }
        });
    }
    
    private void initTablePasien() {
        pColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        pColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        pColumnAlamat.setCellValueFactory(value -> value.getValue().getAlamat());
        pColumnTglLahir.setCellValueFactory(value -> value.getValue().getTanggalLahir());
        pColumnTelepon.setCellValueFactory(value -> value.getValue().getTelepon());
        
//        tablePasien.setItems(pasienRepo.getListPasien());
        tablePasien.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends Pasien> observable, 
                Pasien oldValue, 
                Pasien newValue) -> {
                    showPasienInForm(observable.getValue());
                    disableForms(true);
                    buttonDaftar.setText("Simpan");
                    
        });
    }
    
    private void initCallbacks() {
        kunjunganCallback = new KunjunganRepository.Callback() {
            @Override
            public void onLoadListKunjunganSucceed() {
                progressBar.setVisible(false);                
                container.setDisable(false);
                tableKunjungan.setItems(kunjunganRepo.getListKunjungan());
            }

            @Override
            public void onInsertNewKunjunganSucceed() {
                progressBar.setVisible(false);                
                container.setDisable(false);
                cancelFormKunjunganPasien();
                dialogBuilder.showSuccessDialog("Berhasil mendaftarkan pasien.");
            }

            @Override
            public void onDeleteKunjunganSucceed() {
                progressBar.setVisible(false);                
                container.setDisable(false);
                dialogBuilder.showSuccessDialog("Berhasil menghapus data kunjungan.");
            }

            @Override public void onGetKunjunganTepatWaktuSucceed(ObservableList<Laporan> list) { }
            @Override public void onGetKunjunganTidakTepatSucceed(ObservableList<Laporan> list) { }
            @Override public void onGetListKunjunganKemarinSucceed(ObservableList<KunjunganPasien> list) { }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                progressBar.setVisible(false);                
                container.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
        
        pasienCallback = new PasienRepository.Callback() {
            @Override
            public void onLoadListPasienSucceed() {
                tablePasien.setItems(pasienRepo.getListPasien());
                progressBar.setVisible(false);                
                container.setDisable(false);
            }

            @Override
            public void onInsertPasienSucceed() { }
            @Override
            public void onDeletePasienSucceed() { }
            @Override
            public void onUpdatePasienSucceed(Pasien pasien) { }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                progressBar.setVisible(false);                
                container.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }
    
    private void showPasienInForm(Pasien pasien) {
        if (pasien != null) {
            formNoRM.setText(pasien.getNoRM().get());
            formNama.setText(pasien.getNama().get());
            formAlamat.setText(pasien.getAlamat().get());
            selectedPasien = pasien;
        }
    }
    
    private void clearForms() {
        formNoRM.setText("");
        formNama.setText("");
        formAlamat.setText("");
    }
    
    private void disableForms(boolean isDisable) {
        formNoRM.setDisable(isDisable);
        formNama.setDisable(isDisable);
        formAlamat.setDisable(isDisable);
    }
}
