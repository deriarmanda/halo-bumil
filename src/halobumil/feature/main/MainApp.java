/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.main;

import gnu.io.CommPortIdentifier;
import halobumil.config.AppConfig;
import halobumil.feature.akun.login.LoginController;
import halobumil.feature.home.HomeController;
import halobumil.util.DialogBuilder;
import halobumil.util.LoggerUtils;
import java.io.IOException;
import java.util.Enumeration;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author DERI
 */
public class MainApp extends Application {
    
    public static final int LOGIN_PAGE = 110,
            HOME_PAGE = 111;
    private final String TAG_NAME = MainApp.class.getSimpleName();
    
    private AppConfig appConfig;
    private Stage mainStage;
    private BorderPane root;
    private Node loginView, homeView;
    private HomeController homeController;
    
    @Override
    public void start(Stage primaryStage) {
        
        DialogBuilder.init(mainStage);
        Thread.setDefaultUncaughtExceptionHandler(MainApp::showError);
        
        appConfig = AppConfig.getInstance();
        mainStage = primaryStage;
        mainStage.setTitle(appConfig.APP_NAME);
        mainStage.getIcons().add(appConfig.APP_ICON);
        
        initContentView();
        initRootView();
        showPage(LOGIN_PAGE);
        
        Scene scene = new Scene(root);
        mainStage.setScene(scene);
        mainStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop(); 
        appConfig.setRunning(false);
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);    
    }
    
    private static void showError(Thread t, Throwable e) {
        System.err.println("***Default exception handler***");
        if (Platform.isFxApplicationThread()) {
            DialogBuilder.getInstance().showErrorDialog(e.getMessage());
        } else {
            System.err.println("An unexpected error occurred in "+t);
            e.printStackTrace();
        }
    }
    
    public void showPage(int page) {
        switch (page) {
            case LOGIN_PAGE:
                Enumeration enums = CommPortIdentifier.getPortIdentifiers();
                while (enums.hasMoreElements()) System.out.println("2 "+enums.nextElement().getClass().getName());
                root.setCenter(loginView);
                break;
            case HOME_PAGE:
                root.setCenter(homeView);
                homeController.onPageShown();
                break;
            default:
                break;
        }
    }
    
    private void initRootView() {        
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("Root.fxml"));
        try {
            root = loader.load();
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Root.fxml file : \n"+ex
            );
            root = new BorderPane();
        }
    }
    private void initContentView() {
        FXMLLoader loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/akun/login/Login.fxml")
        );
        
        // Login
        try {
            loginView = loader.load();
            LoginController controller = (LoginController) loader.getController();
            controller.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Login.fxml file : \n"+ex
            );
            loginView = new BorderPane();
        }
        
        // Home
        loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/home/Home.fxml")
        );
        try {
            homeView = loader.load();
            homeController = (HomeController) loader.getController();
            homeController.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Login.fxml file : \n"+ex
            );
            homeView = new BorderPane();
        }
    }
}
