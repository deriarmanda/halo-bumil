/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.entridata.detail;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.Pasien;
import halobumil.data.model.SMS;
import halobumil.data.model.Trimester;
import halobumil.data.repository.AccountRepository;
import halobumil.data.repository.PasienRepository;
import halobumil.data.repository.SMSRepository;
import halobumil.data.repository.TrimesterRepository;
import halobumil.feature.entridata.EntriDataController;
import halobumil.util.DateTimeUtils;
import halobumil.util.DialogBuilder;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class DetailController implements Initializable {

    // General Usage
    @FXML private GridPane container;
    
    // Form Pasien Usage
    @FXML private JFXTextField formNoRM;
    @FXML private JFXTextField formNama;
    @FXML private JFXDatePicker formTglLahir;
    @FXML private JFXTextField formAlamat;
    @FXML private JFXTextField formTelepon;
    @FXML private JFXButton buttonSave;
    @FXML private JFXButton buttonCancel;
    
    // Send SMS Usage
    @FXML private TextArea formMessage;
    
    // Insert Trimester Usage
    @FXML private JFXComboBox spinnerTrimesterType;
    @FXML private JFXDatePicker formTrimesterDate;
    
    // Table Trimester Usage
    @FXML private TableView<Trimester> tableTrimester1;
    @FXML private TableColumn<Trimester, String> t1ColumnDate;
    @FXML private TableColumn<Trimester, String> t1ColumnPetugas;
    @FXML private TableView<Trimester> tableTrimester2;
    @FXML private TableColumn<Trimester, String> t2ColumnDate;
    @FXML private TableColumn<Trimester, String> t2ColumnPetugas;
    @FXML private TableView<Trimester> tableTrimester3;
    @FXML private TableColumn<Trimester, String> t3ColumnDate;
    @FXML private TableColumn<Trimester, String> t3ColumnPetugas;
    
    private EntriDataController parentController;
    private PasienRepository pasienRepo;
    private PasienRepository.Callback pasienCallback;
    private Pasien pasien;
    private TrimesterRepository trimesterRepo;
    private TrimesterRepository.Callback trimesterCallback;
    private SMSRepository smsRepo;
    private SMSRepository.Callback smsCallback;
    private AccountRepository accountRepo;
    private DialogBuilder dialogBuilder;
    @FXML
    private JFXTextField formG;
    @FXML
    private JFXTextField formP;
    @FXML
    private JFXTextField formA;
    @FXML
    private JFXTextField formHaid;
    @FXML
    private JFXTextField formHpht;
    @FXML
    private JFXTextField formHpl;
    @FXML
    private JFXTextField formBB;
    @FXML
    private JFXTextField formMual;
    @FXML
    private JFXTextField formPusing;
    @FXML
    private JFXTextField formNyeri;
    @FXML
    private JFXTextField formGerakJanin;
    @FXML
    private JFXTextField formOedema;
    @FXML
    private JFXTextField formNafsuMakan;
    @FXML
    private JFXTextField formPendarahan;
    @FXML
    private JFXTextField formPenyakit;
    @FXML
    private JFXTextField formRiwayatPenyakit;
    @FXML
    private JFXTextField formKebiasaanIbu;
    @FXML
    private JFXTextField formStatusTT;
    @FXML
    private JFXTextField formHivAids;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pasienRepo = PasienRepository.getInstance();
        trimesterRepo = TrimesterRepository.getInstance();
        accountRepo = AccountRepository.getInstance();
        smsRepo = SMSRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        
        spinnerTrimesterType.getItems().clear();
        spinnerTrimesterType.getItems().addAll(
                "Trimester 1", 
                "Trimester 2",
                "Trimester 3"
        );
        
        initCallbacks();
        initTableTrimester();
    }    
    
    public void onPageShown(Pasien pasien) {
        parentController.showProgressIndicator(true);
        container.setDisable(true);
        trimesterRepo.loadListTrimester1(pasien.getUid().getValue(), trimesterCallback);
        trimesterRepo.loadListTrimester2(pasien.getUid().getValue(), trimesterCallback);
        trimesterRepo.loadListTrimester3(pasien.getUid().getValue(), trimesterCallback);
        
        this.pasien = pasien;
        fetchPasien();        
    }

    public void setParentController(EntriDataController parentController) {
        this.parentController = parentController;
    }
    
    private void fetchPasien() {
        formNoRM.setText(pasien.getNoRM().get());
        formNama.setText(pasien.getNama().get());
        formTglLahir.setValue(DateTimeUtils.parseDateOnly(
                pasien.getTanggalLahir().get())
        );
        formAlamat.setText(pasien.getAlamat().get());
        formTelepon.setText(pasien.getTelepon().get());
        
        formG.setText(pasien.getDataG().get());
        formP.setText(pasien.getDataP().get());
        formA.setText(pasien.getDataA().get());
        formHaid.setText(pasien.getHaid().get());
        formHpht.setText(pasien.getHpht().get());
        formHpl.setText(pasien.getHpl().get());
        formBB.setText(pasien.getBb().get());
        formMual.setText(pasien.getMual().get());
        formPusing.setText(pasien.getPusing().get());
        formNyeri.setText(pasien.getNyeri().get());
        formGerakJanin.setText(pasien.getGerakJanin().get());
        formOedema.setText(pasien.getOedema().get());
        formNafsuMakan.setText(pasien.getNafsuMakan().get());
        formPendarahan.setText(pasien.getPendarahan().get());
        formPenyakit.setText(pasien.getPenyakit().get());
        formRiwayatPenyakit.setText(pasien.getRiwayatPenyakit().get());
        formKebiasaanIbu.setText(pasien.getKebiasaanIbu().get());
        formStatusTT.setText(pasien.getStatusTT().get());
        formHivAids.setText(pasien.getHivAids().get());
    }
    
    private void enableFormPasien(boolean active) {
        formNoRM.setEditable(active);
        formNama.setEditable(active);
        formTglLahir.setEditable(active);
        formAlamat.setEditable(active);
        formTelepon.setEditable(active);
        
        formG.setEditable(active);
        formP.setEditable(active);
        formA.setEditable(active);
        formHaid.setEditable(active);
        formHpht.setEditable(active);
        formHpl.setEditable(active);
        formBB.setEditable(active);
        formMual.setEditable(active);
        formPusing.setEditable(active);
        formNyeri.setEditable(active);
        formGerakJanin.setEditable(active);
        formOedema.setEditable(active);
        formNafsuMakan.setEditable(active);
        formPendarahan.setEditable(active);
        formPenyakit.setEditable(active);
        formRiwayatPenyakit.setEditable(active);
        formKebiasaanIbu.setEditable(active);
        formStatusTT.setEditable(active);
        formHivAids.setEditable(active);
        
        buttonSave.setDisable(!active);
        buttonCancel.setDisable(!active);
    }
    
    private void initCallbacks() {
        pasienCallback = new PasienRepository.Callback() {
            @Override
            public void onLoadListPasienSucceed() { }
            @Override
            public void onInsertPasienSucceed() { }
            @Override
            public void onDeletePasienSucceed() { }
            @Override
            public void onUpdatePasienSucceed(Pasien editedPasien) {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                
                pasien = editedPasien;
                enableFormPasien(false);
                fetchPasien();
                
                dialogBuilder.showSuccessDialog("Berhasil merubah data identitas pasien.");
            }
                        
            @Override
            public void onRepositoryTaskFailed(String msg) {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
        
        trimesterCallback = new TrimesterRepository.Callback() {
            @Override
            public void onLoadListTrimester1Succeed() {
                tableTrimester1.setItems(trimesterRepo.getListTrimester1());
                parentController.showProgressIndicator(false);
                container.setDisable(false);
            }
            
            @Override
            public void onLoadListTrimester2Succeed() {
                tableTrimester2.setItems(trimesterRepo.getListTrimester2());
                parentController.showProgressIndicator(false);
                container.setDisable(false);
            }
            
            @Override
            public void onLoadListTrimester3Succeed() {
                tableTrimester3.setItems(trimesterRepo.getListTrimester3());
                parentController.showProgressIndicator(false);
                container.setDisable(false);
            }
            
            @Override
            public void onInsertTrimesterSucceed() {
                spinnerTrimesterType.getSelectionModel().clearSelection();
                formTrimesterDate.getEditor().clear();
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showSuccessDialog(AppMessage.INSERT_TRIMESTER_SUCCESS);
            }
            
            @Override
            public void onDeleteTrimesterSucceed() {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showSuccessDialog(AppMessage.DELETE_TRIMESTER_SUCCESS);
            }
            
            @Override
            public void onRepositoryTaskFailed(String msg) {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
        
        smsCallback = new SMSRepository.Callback() {
            @Override
            public void onUpdateSucceed() { }
            @Override
            public void onGetTodaySMSListSucceed(ObservableList<SMS> list) { }
            @Override
            public void onModemConnected(String msg) {
                dialogBuilder.showInfoDialog(msg);
            }

            @Override
            public void onSendSmsSucceed() {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showSuccessDialog("Berhasil mengirimkan SMS.");
            }

            @Override public void onUpdateSmsSucceed() { }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                parentController.showProgressIndicator(false);
                container.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }
    
    private void initTableTrimester() {
        t1ColumnDate.setCellValueFactory(value -> value.getValue().getTanggal());
        t1ColumnPetugas.setCellValueFactory(value -> value.getValue().getPetugas());
        
        t2ColumnDate.setCellValueFactory(value -> value.getValue().getTanggal());
        t2ColumnPetugas.setCellValueFactory(value -> value.getValue().getPetugas());
        
        t3ColumnDate.setCellValueFactory(value -> value.getValue().getTanggal());
        t3ColumnPetugas.setCellValueFactory(value -> value.getValue().getPetugas());
    }
    
    @FXML private void showEntriDataPage() {
        parentController.showPage(EntriDataController.PAGE_FORM);
    }
    
    @FXML private void editPasien() {
        enableFormPasien(true);
    }
    @FXML private void saveEditedPasien() {
        parentController.showProgressIndicator(true);
        container.setDisable(true);
        pasienRepo.updatePasien(
                pasien, 
                formNoRM.getText(),
                formNama.getText(),
                formAlamat.getText(),
                DateTimeUtils.format(formTglLahir.getValue()),
                formTelepon.getText(),
                formG.getText(),
                formP.getText(),
                formA.getText(),
                formHaid.getText(),
                formHpht.getText(),
                formHpl.getText(),
                formBB.getText(),
                formMual.getText(),
                formPusing.getText(),
                formNyeri.getText(),
                formGerakJanin.getText(),
                formOedema.getText(),
                formNafsuMakan.getText(),
                formPendarahan.getText(),
                formPenyakit.getText(),
                formRiwayatPenyakit.getText(),
                formKebiasaanIbu.getText(),
                formStatusTT.getText(),
                formHivAids.getText(),
                pasienCallback
        );
    }
    @FXML private void cancelEditingPasien() {
        enableFormPasien(false);
        fetchPasien();
    }
    
    @FXML private void sendSMS() {
        parentController.showProgressIndicator(true);
        container.setDisable(true);
        
        smsRepo.sendSms(
                pasien.getTelepon().get(), 
                formMessage.getText(), 
                smsCallback
        );
    }
    
    @FXML private void addTrimesterDate() {
        if (formTrimesterDate.getValue() != null) {
            Trimester trimester = new Trimester(
                    -1,
                    pasien.getUid().getValue(),
                    DateTimeUtils.format(formTrimesterDate.getValue()),
                    accountRepo.getCurrentAkun().getUsername(),
                    1
            );
            
            parentController.showProgressIndicator(true);
            container.setDisable(true);
            switch (spinnerTrimesterType.getSelectionModel().getSelectedIndex()) {
                case 0:
                    trimester.setTipe(1);
                    trimesterRepo.insertTrimester1(trimester, trimesterCallback);
                    break;
                case 1:
                    trimester.setTipe(2);
                    trimesterRepo.insertTrimester2(trimester, trimesterCallback);
                    break;
                case 2:
                    trimester.setTipe(3);
                    trimesterRepo.insertTrimester3(trimester, trimesterCallback);
                    break;
                default:
                    parentController.showProgressIndicator(false);
                    container.setDisable(false);
                    dialogBuilder.showErrorDialog("Form Tipe Trimester harus dipilih !");
                    break;
            }
        } else {
            dialogBuilder.showErrorDialog("Form Tanggal tidak boleh kosong !");
        }
    }
    
    @FXML private void deleteSelectedTrimester1() {
        Trimester selected = tableTrimester1.getSelectionModel().getSelectedItem();
        if (selected != null) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_TRIMESTER_CONFIRMATION, () -> {
                        parentController.showProgressIndicator(true);
                        container.setDisable(true);
                        trimesterRepo.deleteTrimester1(selected, trimesterCallback);
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    
    @FXML private void deleteSelectedTrimester2() {
        Trimester selected = tableTrimester2.getSelectionModel().getSelectedItem();
        if (selected != null) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_TRIMESTER_CONFIRMATION, () -> {
                        parentController.showProgressIndicator(false);
                        container.setDisable(false);
                        trimesterRepo.deleteTrimester2(selected, trimesterCallback);
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    
    @FXML private void deleteSelectedTrimester3() {
        Trimester selected = tableTrimester3.getSelectionModel().getSelectedItem();
        if (selected != null) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_TRIMESTER_CONFIRMATION, () -> {
                        parentController.showProgressIndicator(false);
                        container.setDisable(false);
                        trimesterRepo.deleteTrimester3(selected, trimesterCallback);
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
}
