/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.entridata;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.KunjunganPasien;
import halobumil.data.model.Laporan;
import halobumil.data.model.Pasien;
import halobumil.data.repository.KunjunganRepository;
import halobumil.data.repository.PasienRepository;
import halobumil.feature.entridata.detail.DetailController;
import halobumil.util.DateTimeUtils;
import halobumil.util.DialogBuilder;
import halobumil.util.LoggerUtils;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class EntriDataController implements Initializable {

    public static final int PAGE_FORM = 210, PAGE_DETAIL = 211;
    private final String TAG_NAME = EntriDataController.class.getSimpleName();
    
    // General & Layouting Usage
    @FXML private BorderPane root;
    @FXML private ProgressBar progressBar;
    @FXML private GridPane rootForm;
    private GridPane rootDetail;
    private DetailController detailController;
    
    // Form Identitas Usage
    @FXML private JFXTextField formNoRM;
    @FXML private JFXTextField formNama;
    @FXML private JFXDatePicker formTglLahir;
    @FXML private JFXTextField formAlamat;
    @FXML private JFXTextField formTelepon;
    @FXML private JFXButton buttonSimpan;
    @FXML private JFXButton buttonBatal;
    
    // Table Kunjungan Pasien Usage
    @FXML private TableView<KunjunganPasien> tableKunjungan;
    @FXML private TableColumn<KunjunganPasien, String> kColumnPukul;
    @FXML private TableColumn<KunjunganPasien, String> kColumnNama;
    @FXML private TableColumn<KunjunganPasien, String> kColumnAlamat;
    
    // Table Pasien Usage
    @FXML private TableView<Pasien> tablePasien;
    @FXML private TableColumn<Pasien, String> pColumnNoRM;
    @FXML private TableColumn<Pasien, String> pColumnNama;
    @FXML private TableColumn<Pasien, String> pColumnAlamat;
    @FXML private TableColumn<Pasien, String> pColumnTglLahir;
    @FXML private TableColumn<Pasien, String> pColumnTelepon;
    @FXML private JFXTextField formSearchPasien;
    @FXML private StackPane buttonDetail;
    @FXML private StackPane buttonHapus;
    
    private KunjunganRepository kunjunganRepo;
    private KunjunganRepository.Callback kunjunganCallback;
    private KunjunganPasien selectedKunjungan;
    private PasienRepository pasienRepo;
    private PasienRepository.Callback pasienCallback;
    private Pasien selectedPasien;
    private DialogBuilder dialogBuilder;
    @FXML
    private JFXTextField formG;
    @FXML
    private JFXTextField formP;
    @FXML
    private JFXTextField formA;
    @FXML
    private JFXTextField formHaid;
    @FXML
    private JFXTextField formHpht;
    @FXML
    private JFXTextField formHpl;
    @FXML
    private JFXTextField formBB;
    @FXML
    private JFXTextField formMual;
    @FXML
    private JFXTextField formPusing;
    @FXML
    private JFXTextField formNyeri;
    @FXML
    private JFXTextField formGerakJanin;
    @FXML
    private JFXTextField formOedema;
    @FXML
    private JFXTextField formNafsuMakan;
    @FXML
    private JFXTextField formPendarahan;
    @FXML
    private JFXTextField formPenyakit;
    @FXML
    private JFXTextField formRiwayatPenyakit;
    @FXML
    private JFXTextField formKebiasaanIbu;
    @FXML
    private JFXTextField formStatusTT;
    @FXML
    private JFXTextField formHivAids;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO      
        initDetailsPage();
        kunjunganRepo = KunjunganRepository.getInstance();
        pasienRepo = PasienRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        selectedPasien = null;
        
        initCallbacks();
        initTableKunjungan();
        initTablePasien();
    } 
    
    public void onPageShown() {
        progressBar.setVisible(true);                
        root.setDisable(true);
        kunjunganRepo.reloadListKunjungan(kunjunganCallback);
        pasienRepo.reloadListPasien(pasienCallback);
    }
    
    public void showPage(int page) {
        switch (page) {
            case PAGE_FORM: 
                onPageShown();
                root.setCenter(rootForm);
                break;
            case PAGE_DETAIL: 
                detailController.onPageShown(selectedPasien);
                root.setCenter(rootDetail);
                break;
            default:
                break;
        }
    }
    
    public void showProgressIndicator(boolean active) {
        progressBar.setVisible(active);
    }

    @FXML
    private void savePasienBaru() {
        progressBar.setVisible(true);
        root.setDisable(true);
        Pasien p = new Pasien(
                -1,
                formNoRM.getText(),
                formNama.getText(),
                formAlamat.getText(),
                DateTimeUtils.format(formTglLahir.getValue() == null? LocalDate.now() : formTglLahir.getValue()),
                "+62"+formTelepon.getText(),
                formG.getText(),
                formP.getText(),
                formA.getText(),
                formHaid.getText(),
                formHpht.getText(),
                formHpl.getText(),
                formBB.getText(),
                formMual.getText(),
                formPusing.getText(),
                formNyeri.getText(),
                formGerakJanin.getText(),
                formOedema.getText(),
                formNafsuMakan.getText(),
                formPendarahan.getText(),
                formPenyakit.getText(),
                formRiwayatPenyakit.getText(),
                formKebiasaanIbu.getText(),
                formStatusTT.getText(),
                formHivAids.getText()
        );
        if (selectedKunjungan != null) {
            pasienRepo.insertAndUpdatePasienBaru(
                    selectedKunjungan.getUid().get(), 
                    p, 
                    pasienCallback,
                    kunjunganCallback
            );
        } else {
            pasienRepo.insertAndUpdatePasienBaru(-1, p, pasienCallback, kunjunganCallback);
        }
    }
    @FXML
    private void clearForms() {
        formNoRM.clear();
        formNama.clear();
        formAlamat.clear();
        formTglLahir.getEditor().clear();
        formTelepon.clear();
        
        formG.clear();
        formP.clear();
        formA.clear();
        formHaid.clear();
        formHpht.clear();
        formHpl.clear();
        formBB.clear();
        formMual.clear();
        formPusing.clear();
        formNyeri.clear();
        formGerakJanin.clear();
        formOedema.clear();
        formNafsuMakan.clear();
        formPendarahan.clear();
        formPenyakit.clear();
        formRiwayatPenyakit.clear();
        formKebiasaanIbu.clear();
        formStatusTT.clear();
        formHivAids.clear();
    }
    @FXML
    private void searchPasien() {
        String query = formSearchPasien.getText();
        tablePasien.getItems().stream().filter(
                item -> item.getNoRM().get().contains(query)
        ).findAny().ifPresent(item -> {
            tablePasien.requestFocus();
            tablePasien.getSelectionModel().select(item);
            tablePasien.scrollTo(item);
        });
    }
    @FXML
    private void deletePasien() {
        if (selectedPasien != null) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_PASIEN_CONFIRMATION, () -> {
                        progressBar.setVisible(true);                
                        root.setDisable(true);
                        pasienRepo.deletePasien(selectedPasien, pasienCallback);
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    @FXML
    private void showPasienDetails() {
        if (selectedPasien != null) {
            showPage(PAGE_DETAIL);
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    @FXML
    private void hoverFab(MouseEvent event) {
        if (event.getSource() == buttonDetail) {
            DropShadow effect = (DropShadow) buttonDetail.getEffect();            
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
                effect.setWidth(0.0);
                effect.setHeight(0.0);
            } else {
                effect.setWidth(20.0);
                effect.setHeight(20.0);
            }
        }
        
        if (event.getSource() == buttonHapus) {
            DropShadow effect = (DropShadow) buttonHapus.getEffect();
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
                effect.setWidth(0.0);
                effect.setHeight(0.0);
            } else {
                effect.setWidth(20.0);
                effect.setHeight(20.0);
            }
        }
    }
    
    private void initDetailsPage() {
        FXMLLoader loader = new FXMLLoader(
                EntriDataController.class
                        .getResource("/halobumil/feature/entridata/detail/Detail.fxml")
        );
        
        try {
            rootDetail = loader.load();
            detailController = loader.getController();
            detailController.setParentController(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Detail.fxml file : \n"+ex
            );
            rootDetail = new GridPane();
        }
    }
    
    private void initTableKunjungan() {
        kColumnPukul.setCellValueFactory(value -> value.getValue().getWaktu());
        kColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        kColumnAlamat.setCellValueFactory(value -> value.getValue().getAlamat());
        
//        tableKunjungan.setItems(kunjunganRepo.getWaitingListKunjungan());
        tableKunjungan.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends KunjunganPasien> observable, 
                KunjunganPasien oldValue, 
                KunjunganPasien newValue) -> {
                    selectedKunjungan = observable.getValue();
                    showSelectedKunjungan(selectedKunjungan);
        });
    }
    
    private void initTablePasien() {
        pColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        pColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        pColumnAlamat.setCellValueFactory(value -> value.getValue().getAlamat());
        pColumnTglLahir.setCellValueFactory(value -> value.getValue().getTanggalLahir());
        pColumnTelepon.setCellValueFactory(value -> value.getValue().getTelepon());
        
//        tablePasien.setItems(pasienRepo.getListPasien());
        tablePasien.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends Pasien> observable, 
                Pasien oldValue, 
                Pasien newValue) -> {
                    selectedPasien = observable.getValue();
        });
        tablePasien.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    showPasienDetails();
                }
            }
        });
    }
    
    private void initCallbacks() {
        kunjunganCallback = new KunjunganRepository.Callback() {
            @Override
            public void onLoadListKunjunganSucceed() {
                tableKunjungan.setItems(kunjunganRepo.getWaitingListKunjungan());
                progressBar.setVisible(false);                
                root.setDisable(false);
            }

            @Override public void onInsertNewKunjunganSucceed() { }
            @Override public void onDeleteKunjunganSucceed() { }
            @Override public void onGetKunjunganTepatWaktuSucceed(ObservableList<Laporan> list) { }
            @Override public void onGetKunjunganTidakTepatSucceed(ObservableList<Laporan> list) { }
            @Override public void onGetListKunjunganKemarinSucceed(ObservableList<KunjunganPasien> list) { }
            @Override
            public void onRepositoryTaskFailed(String msg) { 
                progressBar.setVisible(false);                
                root.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
        
        pasienCallback = new PasienRepository.Callback() {
            @Override
            public void onLoadListPasienSucceed() {
                tablePasien.setItems(pasienRepo.getListPasien());
                progressBar.setVisible(false);                
                root.setDisable(false);
            }

            @Override
            public void onInsertPasienSucceed() {
                progressBar.setVisible(false);                
                root.setDisable(false);
                dialogBuilder.showSuccessDialog("Berhasil menyimpan data identitas pasien.");
            }

            @Override
            public void onDeletePasienSucceed() {
                progressBar.setVisible(false);                
                root.setDisable(false);
                dialogBuilder.showSuccessDialog("Berhasil menghapus semua data pasien bersangkutan.");
            }
            @Override
            public void onUpdatePasienSucceed(Pasien pasien) { }       

            @Override
            public void onRepositoryTaskFailed(String msg) {
                progressBar.setVisible(false);                
                root.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }
    
    private void showSelectedKunjungan(KunjunganPasien k) {
        if (k != null) {
            formNama.setText(k.getNama().get());
            formAlamat.setText(k.getAlamat().get());
        }
    }
}
