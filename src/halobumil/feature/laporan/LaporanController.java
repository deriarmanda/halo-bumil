/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.laporan;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXProgressBar;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.KunjunganPasien;
import halobumil.data.model.Laporan;
import halobumil.data.repository.KunjunganRepository;
import halobumil.util.DialogBuilder;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class LaporanController implements Initializable {

    // General Usage
    @FXML private GridPane root;
    @FXML private JFXProgressBar progressBar;
    
    // Filtering Usage
    @FXML private JFXComboBox spinnerMonth;
    @FXML private JFXComboBox spinnerYear;
    
    // Result Usage
    @FXML private Label textTotalPasien;
    @FXML private Label textPasienTepat;
    @FXML private Label textPasienTidakTepat;
    
    // Table Pasien Tepat Usage
    @FXML private TableView<Laporan> tableTepat;
    @FXML private TableColumn<Laporan, String> tColumnTanggal;
    @FXML private TableColumn<Laporan, String> tColumnNoRM;
    @FXML private TableColumn<Laporan, String> tColumnNama;
    
    // Table Pasien Tidak Tepat Usage
    @FXML private TableView<Laporan> tableTidakTepat;
    @FXML private TableColumn<Laporan, String> ttColumnTglKunjungan;
    @FXML private TableColumn<Laporan, String> ttColumnTglTrimester;
    @FXML private TableColumn<Laporan, String> ttColumnNoRM;
    @FXML private TableColumn<Laporan, String> ttColumnNama;
    
    private final ObservableList<Laporan> listTepat = FXCollections.observableArrayList();
    private final ObservableList<Laporan> listTidakTepat = FXCollections.observableArrayList();
    private KunjunganRepository kunjunganRepo;
    private KunjunganRepository.Callback kunjunganCallback;
    private DialogBuilder dialogBuilder;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        kunjunganRepo = KunjunganRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        
        initTables();
        initSpinners();
        initCallbacks();
    }
    
    public void onPageShown() {
        spinnerMonth.getSelectionModel().clearSelection();
        spinnerYear.getSelectionModel().clearSelection();
        tableTepat.getItems().clear();
        tableTidakTepat.getItems().clear();
    }
    
    private void initTables() {
        tColumnTanggal.setCellValueFactory(value -> value.getValue().getTglKunjungan());
        tColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        tColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        tableTepat.setItems(listTepat);
        
        ttColumnTglKunjungan.setCellValueFactory(value -> value.getValue().getTglKunjungan());
        ttColumnTglTrimester.setCellValueFactory(value -> value.getValue().getTglTrimester());
        ttColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        ttColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        tableTidakTepat.setItems(listTidakTepat);
    }
    
    private void initCallbacks() {
        kunjunganCallback = new KunjunganRepository.Callback() {
            @Override
            public void onLoadListKunjunganSucceed() { }
            @Override
            public void onInsertNewKunjunganSucceed() { }
            @Override
            public void onDeleteKunjunganSucceed() { }
            @Override
            public void onGetKunjunganTepatWaktuSucceed(ObservableList<Laporan> list) {
                listTepat.clear();
                listTepat.addAll(list);
                
                String totPasien = textTotalPasien.getText();
                int currPasien = Integer.parseInt(
                        totPasien.substring(0, totPasien.indexOf(" "))
                );
                textTotalPasien.setText((currPasien + list.size()) + " Orang");
                textPasienTepat.setText(list.size() + " Orang");
                
                progressBar.setVisible(false);
                root.setDisable(false);
            }

            @Override
            public void onGetKunjunganTidakTepatSucceed(ObservableList<Laporan> list) {
                listTidakTepat.clear();
                listTidakTepat.addAll(list);
                
                String totPasien = textTotalPasien.getText();
                int currPasien = Integer.parseInt(
                        totPasien.substring(0, totPasien.indexOf(" "))
                );
                textTotalPasien.setText((currPasien + list.size()) + " Orang");
                textPasienTidakTepat.setText(list.size() + " Orang");
                
                progressBar.setVisible(false);
                root.setDisable(false);
            }

            @Override public void onGetListKunjunganKemarinSucceed(ObservableList<KunjunganPasien> list) { }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                progressBar.setVisible(false);
                root.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }

    private void initSpinners() {
        spinnerMonth.getItems().clear();
        spinnerMonth.getItems().addAll(
                "Semua Bulan",
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
        );
        spinnerYear.getItems().clear();
        spinnerYear.getItems().addAll(
                "2018", "2019", "2020", "2021", "2022",
                "2023", "2024", "2025", "2026", "2027",
                "2028", "2029", "2030", "2031", "2032",
                "2033", "2034", "2035", "2036", "2037",
                "2038", "2039", "2040"
        );
    }
    
    @FXML private void doReporting() {
        int month = spinnerMonth.getSelectionModel().getSelectedIndex();
        String selectedYear = (String) spinnerYear.getSelectionModel()
                .getSelectedItem();
        if (month < 0 || selectedYear == null) {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        } else {
            String selectedMonth = (month == 0? "ALL" : (month<10? "0"+month : ""+month));
            textTotalPasien.setText("0 Orang");
            textPasienTepat.setText("0 Orang");
            textPasienTidakTepat.setText("0 Orang");
            kunjunganRepo.getKunjunganTepatWaktu(
                    selectedMonth, 
                    selectedYear, 
                    kunjunganCallback
            );
            kunjunganRepo.getKunjunganTidakTepat(
                    selectedMonth, 
                    selectedYear, 
                    kunjunganCallback
            );
        }
    }
}
