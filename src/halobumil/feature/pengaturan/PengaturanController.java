/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.pengaturan;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.Akun;
import halobumil.data.model.SMS;
import halobumil.data.repository.AccountRepository;
import halobumil.data.repository.SMSRepository;
import halobumil.util.DialogBuilder;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class PengaturanController implements Initializable {

    // General Usage
    @FXML private AnchorPane root;
    @FXML private JFXProgressBar progressBar;
    
    // Active Account Usage
    @FXML private JFXTextField formUsername;
    @FXML private JFXPasswordField formPassword;
    @FXML private JFXButton buttonSaveUser;
    
    // Create Account Usage
    @FXML private JFXTextField formNewUsername;
    @FXML private JFXPasswordField formNewPassword;
    @FXML private JFXButton buttonCreateUser;
    
    // SMS Gateway Editor Usage
    @FXML private TextArea formSMSKemarin;
    @FXML private TextArea formSMSPagi;
    @FXML private TextArea formSMSSore;
    @FXML private JFXButton buttonSaveSMS;
    
    private AccountRepository accountRepo;
    private SMSRepository smsRepo;
    private SMSRepository.Callback smsCallback;
    private DialogBuilder dialogBuilder;
    @FXML
    private ComboBox<String> comboPort;
    @FXML
    private Label textServer;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        accountRepo = AccountRepository.getInstance();
        smsRepo = SMSRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        
        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            textServer.setText(ip+" / 9999");
        } catch (UnknownHostException ex) {
            Logger.getLogger(PengaturanController.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboPort.getItems().clear();
        for(int i=1; i<=20; i++) comboPort.getItems().add("COM"+i);
        
        smsCallback = new SMSRepository.Callback() {
            @Override
            public void onUpdateSucceed() {
                fetchDataSMS();
                progressBar.setVisible(false);
                root.setDisable(false);
                dialogBuilder.showSuccessDialog("Berhasil merubah data SMS Gateway. Silahkan restart aplikasi untuk menghubungkan ulang ke modem.");
            }

            @Override
            public void onGetTodaySMSListSucceed(ObservableList<SMS> list) { }
            @Override public void onModemConnected(String msg) { }
            @Override public void onSendSmsSucceed() { }
            @Override public void onUpdateSmsSucceed() { }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                progressBar.setVisible(false);
                root.setDisable(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }     
    
    public void onPageShown() {
        Akun akun = accountRepo.getCurrentAkun();
        formUsername.setText(akun.getUsername());
        formPassword.setText(akun.getPassword());
        fetchDataSMS();
    }
    
    private void fetchDataSMS() {
        formSMSKemarin.setText(smsRepo.getTeksKemarin());
        formSMSPagi.setText(smsRepo.getTeksPagi());
        formSMSSore.setText(smsRepo.getTeksSore()); 
        comboPort.getSelectionModel().select(smsRepo.getComPort());
    }
    
    @FXML private void saveEditedAccount() {
        String username = formUsername.getText(), 
                password = formPassword.getText();
        
        String msgResult = accountRepo
                .updateCurrentAccount(username, password);
        if (msgResult.equals(AppMessage.UPDATE_ACCOUNT_SUCCESS))
            dialogBuilder.showSuccessDialog(msgResult);
        else dialogBuilder.showErrorDialog(msgResult);
    }
    @FXML private void createNewAccount() {
        String username = formNewUsername.getText(), 
                password = formNewPassword.getText();
        
        String msgResult = accountRepo
                .createNewAccount(username, password, false);
        if (msgResult.equals(AppMessage.REGISTER_SUCCESS)) 
            dialogBuilder.showSuccessDialog(msgResult);
        else dialogBuilder.showErrorDialog(msgResult);
    }
    @FXML private void saveEditedSMS() {
        progressBar.setVisible(true);
        root.setDisable(true);
        smsRepo.updateDataSMS(
                formSMSKemarin.getText(), 
                formSMSPagi.getText(), 
                formSMSSore.getText(), 
                comboPort.getValue(),
                smsCallback
        );
    }
}
