/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.akun.login;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import halobumil.assets.string.AppMessage;
import halobumil.data.repository.AccountRepository;
import halobumil.feature.main.MainApp;
import halobumil.util.DialogBuilder;
import halobumil.util.SmsSenderThread;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class LoginController implements Initializable {

    @FXML
    private JFXTextField formUsername;
    @FXML
    private JFXPasswordField formPassword;
    @FXML
    private JFXButton buttonLogin;
    @FXML
    private JFXButton buttonRegister;
    
    private MainApp mainApp;
    private AccountRepository accountRepo;
    private DialogBuilder dialogBuilder;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        accountRepo = AccountRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
    }    
    
    public void setMainApp(MainApp app) {
        mainApp = app;
    }
    
    private void clearForms() {
        formUsername.clear();
        formPassword.clear();
    }
    
    @FXML
    private void doLogin() {
        String username = formUsername.getText(), 
                password = formPassword.getText();
        
        String msgResult = accountRepo
                .checkAccountInDatabase(username, password);
        if (msgResult.equals(AppMessage.LOGIN_SUCCESS)) {
            clearForms();
            dialogBuilder.showSuccessDialog(msgResult);
            mainApp.showPage(MainApp.HOME_PAGE);
            SmsSenderThread.getInstance().start();
        } else dialogBuilder.showErrorDialog(msgResult);
    }
    @FXML
    private void doRegister() {
        String username = formUsername.getText(), 
                password = formPassword.getText();
        
        String msgResult = accountRepo
                .createNewAccount(username, password, true);
        if (msgResult.equals(AppMessage.REGISTER_SUCCESS)) {
            clearForms();
            dialogBuilder.showSuccessDialog(msgResult);
            mainApp.showPage(MainApp.HOME_PAGE);
            SmsSenderThread.getInstance().start();
        } else dialogBuilder.showErrorDialog(msgResult);
    }
    @FXML 
    private void forceLogin() {
        //mainApp.showPage(MainApp.HOME_PAGE);
    }
}
