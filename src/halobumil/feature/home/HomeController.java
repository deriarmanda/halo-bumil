/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.feature.home;

import com.jfoenix.controls.JFXProgressBar;
import halobumil.assets.string.AppMessage;
import halobumil.data.model.KunjunganPasien;
import halobumil.data.model.Laporan;
import halobumil.data.model.SMS;
import halobumil.data.repository.AccountRepository;
import halobumil.data.repository.KunjunganRepository;
import halobumil.data.repository.SMSRepository;
import halobumil.feature.entridata.EntriDataController;
import halobumil.feature.laporan.LaporanController;
import halobumil.feature.main.MainApp;
import halobumil.feature.pendaftaran.PendaftaranPasienController;
import halobumil.feature.pengaturan.PengaturanController;
import halobumil.util.DateTimeUtils;
import halobumil.util.DialogBuilder;
import halobumil.util.LoggerUtils;
import halobumil.util.SmsSenderTask;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class HomeController implements Initializable {

    private final String TAG_NAME = HomeController.class.getSimpleName(),
            IMG_HOME_PATH = "url('halobumil/assets/image/ic_home_colored_40px.png')",
            IMG_PENDAFTARAN_PATH = "url('halobumil/assets/image/ic_pendaftaran_colored_40px.png')",
            IMG_ENTRI_DATA_PATH = "url('halobumil/assets/image/ic_entri_data_colored_40px.png')",
            IMG_LAPORAN_PATH = "url('halobumil/assets/image/ic_laporan_colored_40px.png')",
            IMG_PENGATURAN_PATH = "url('halobumil/assets/image/ic_pengaturan_colored_40px.png')";
    
    // General & Layouting Usage
    @FXML private BorderPane homeRoot;
    @FXML private GridPane homeView;
    @FXML private JFXProgressBar progressBar;
    @FXML private Label textTitle;
    @FXML private Label textAdmin;
    @FXML private Label textDate;
    
    // Table Kunjungan Hari Ini Usage
    @FXML private TableView<KunjunganPasien> tableToday;
    @FXML private TableColumn<KunjunganPasien, String> tColumnPukul;
    @FXML private TableColumn<KunjunganPasien, String> tColumnNama;
    @FXML private Label textTotalToday;
    
    // Table Kunjungan Kemarin Usage
    @FXML private TableView<KunjunganPasien> tableYesterday;
    @FXML private TableColumn<KunjunganPasien, String> yColumnNoRM;
    @FXML private TableColumn<KunjunganPasien, String> yColumnNama;
    @FXML private Label textTotalYesterday;
    
    // Table List SMS Hari Ini Usage
    @FXML private TableView<SMS> tableSMS;
    @FXML private TableColumn<SMS, String> sColumnNama;
    @FXML private TableColumn<SMS, String> sColumnTelepon;
    @FXML private TableColumn<SMS, String> sColumnTanggal;
    @FXML private TableColumn<SMS, String> sColumnStatus;
    
    private MainApp mainApp;
    private Node pendaftaranView, entriDataView, laporanView, pengaturanView;
    private PendaftaranPasienController pendaftaranController;
    private EntriDataController entriDataController;
    private LaporanController laporanController;
    private PengaturanController pengaturanController;
    
    private AccountRepository accountRepo;
    private KunjunganRepository kunjunganRepo;
    private SMSRepository smsRepo;
    private KunjunganRepository.Callback kunjunganCallback;
    private SMSRepository.Callback smsCallback;
    private DialogBuilder dialogBuilder;
    
    private ObservableList<KunjunganPasien> listToday, listYesterday;
    private ObservableList<SMS> listSMS;
//    private int selectedSMSIndex;
    
    private SmsSenderTask smsTask;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        accountRepo = AccountRepository.getInstance();
        kunjunganRepo = KunjunganRepository.getInstance();
        smsRepo = SMSRepository.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
//        selectedSMSIndex = -1;
        
        listToday = FXCollections.observableArrayList();
        listYesterday = FXCollections.observableArrayList();
        listSMS = FXCollections.observableArrayList();
        
        initTables();
        initCallbacks();
    } 
    
    public void onPageShown() {
//        if (smsTask == null) smsTask = new SmsSenderTask();
//        if (!smsTask.isRunning) smsTask.start();
        
        textAdmin.setText(accountRepo.getCurrentAkun().getUsername());
        textDate.setText(DateTimeUtils.getCurrentHumanReadableDate());
        openHomePage();        
    }
    
    public void setMainApp(MainApp app) {
        mainApp = app;
        initContentsView();
    }
    
    private void initTables() {
        tColumnPukul.setCellValueFactory(value -> value.getValue().getWaktu());
        tColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        tableToday.setItems(listToday);
        
        yColumnNoRM.setCellValueFactory(value -> value.getValue().getNoRM());
        yColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        tableYesterday.setItems(listYesterday);
        
        sColumnNama.setCellValueFactory(value -> value.getValue().getNama());
        sColumnTelepon.setCellValueFactory(value -> value.getValue().getTelepon());
        sColumnTanggal.setCellValueFactory(value -> value.getValue().getHumanFormatTanggal());
        sColumnStatus.setCellValueFactory(value -> value.getValue().getCurrentStatus());
        tableSMS.setItems(listSMS);
//        tableSMS.getSelectionModel().selectedItemProperty().addListener((
//                ObservableValue<? extends SMS> observable, 
//                SMS oldValue, 
//                SMS newValue) -> {
//                    selectedSMSIndex = tableSMS
//        });
    }
    
    private void initCallbacks() {
        kunjunganCallback = new KunjunganRepository.Callback() {
            @Override
            public void onLoadListKunjunganSucceed() { 
                listToday.clear();
                listToday.addAll(kunjunganRepo.getListKunjungan());
                textTotalToday.setText("Total Pasien : "+listToday.size()+" Orang");
            }

            @Override public void onInsertNewKunjunganSucceed() { }
            @Override public void onDeleteKunjunganSucceed() { }
            @Override public void onGetKunjunganTepatWaktuSucceed(ObservableList<Laporan> list) { }
            @Override public void onGetKunjunganTidakTepatSucceed(ObservableList<Laporan> list) { }
            @Override 
            public void onGetListKunjunganKemarinSucceed(ObservableList<KunjunganPasien> list) { 
                listYesterday.clear();
                listYesterday.addAll(list);
                textTotalYesterday.setText("Total Pasien : "+list.size()+" Orang");
            }
            @Override
            public void onRepositoryTaskFailed(String msg) {
                dialogBuilder.showErrorDialog(msg);
            }
        };
        
        smsCallback = new SMSRepository.Callback() {
            @Override public void onUpdateSucceed() { }
            @Override
            public void onGetTodaySMSListSucceed(ObservableList<SMS> list) {
                listSMS.clear();
                listSMS.addAll(list);
            }

            @Override
            public void onModemConnected(String msg) {
                dialogBuilder.showInfoDialog(msg);
            }
            
            @Override
            public void onSendSmsSucceed() {
                listSMS.clear();
                homeView.setDisable(false);
                progressBar.setVisible(false);
                dialogBuilder.showSuccessDialog("Semua SMS berhasil terkirim.");
            }

            @Override
            public void onUpdateSmsSucceed() {
                homeView.setDisable(false);
                progressBar.setVisible(false);
                dialogBuilder.showSuccessDialog("Berhasil menghapus pasien dari daftar pengiriman SMS.");
                openHomePage();
            }
            
            @Override
            public void onRepositoryTaskFailed(String msg) {
                homeView.setDisable(false);
                progressBar.setVisible(false);
                dialogBuilder.showErrorDialog(msg);
            }
        };
    }
    
    @FXML private void openHomePage() {  
        kunjunganRepo.reloadListKunjungan(kunjunganCallback);
        kunjunganRepo.getListKunjunganKemarin(kunjunganCallback);
        smsRepo.getTodaySMSList(smsCallback);
        showPage(homeView, "Beranda", IMG_HOME_PATH); 
    }
    @FXML private void openPendaftaranPage() {  
        showPage(pendaftaranView, "Pendaftaran Pasien", IMG_PENDAFTARAN_PATH); 
        pendaftaranController.onPageShown();
    }
    @FXML private void openEntriDataPage() {  
        showPage(entriDataView, "Entri Data", IMG_ENTRI_DATA_PATH); 
        entriDataController.onPageShown(); 
    }
    @FXML private void openLaporanPage() { 
        showPage(laporanView, "Laporan Kunjungan", IMG_LAPORAN_PATH); 
        laporanController.onPageShown(); 
    }
    @FXML private void openPengaturanPage() { 
        showPage(pengaturanView, "Pengaturan", IMG_PENGATURAN_PATH);  
        pengaturanController.onPageShown();
    }
    @FXML private void doSignOut() { 
        mainApp.showPage(MainApp.LOGIN_PAGE);
        openHomePage(); 
    }
    @FXML private void sendListSMS() {
        homeView.setDisable(true);
        progressBar.setVisible(true);
        smsRepo.sendSms(listSMS, smsCallback);
    }
    @FXML private void removePasienFromSMSList() {
        int selectedIndex = tableSMS.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            dialogBuilder.showConfirmDialog(AppMessage.DELETE_LIST_SMS_CONFIRMATION, () -> {
                        progressBar.setVisible(true);                
                        homeView.setDisable(true);
                        SMS sms = listSMS.get(selectedIndex);
                        String status = sms.getHumanFormatTanggal().get();
                        if (status.equals("Besok")) sms.setStatusKemarin("Terkirim");
                        else sms.setStatusSore("Terkirim");
                        smsRepo.updateStatusSMS(sms, smsCallback);
                        //listSMS.remove(selectedIndex);
            });
        } else {
            dialogBuilder.showErrorDialog(AppMessage.ERROR_NOTHING_FIELD_SELECTED);
        }
    }
    
    private void initContentsView() {
        FXMLLoader loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/pendaftaran/PendaftaranPasien.fxml")
        );
        
        // Pendaftaran Pasien
        try {
            pendaftaranView = loader.load();
            pendaftaranController = (PendaftaranPasienController) loader.getController();
            //controller.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load PendaftaranPasien.fxml file : \n"+ex
            );
            pendaftaranView = new BorderPane();
        }
        
        // Entri Data
        loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/entridata/EntriData.fxml")
        );
        try {
            entriDataView = loader.load();
            entriDataController = (EntriDataController) loader.getController();
            //controller.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load EntriData.fxml file : \n"+ex
            );
            entriDataView = new BorderPane();
        }
        
        // Laporan Kunjungan
        loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/laporan/Laporan.fxml")
        );
        try {
            laporanView = loader.load();
            laporanController = (LaporanController) loader.getController();
            //controller.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Laporan.fxml file : \n"+ex
            );
            laporanView = new BorderPane();
        }
        
        // Pengaturan
        loader = new FXMLLoader(
                MainApp.class.getResource("/halobumil/feature/pengaturan/Pengaturan.fxml")
        );
        try {
            pengaturanView = loader.load();
            pengaturanController = loader.getController();
            //controller.setMainApp(this);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(
                    TAG_NAME, 
                    "Error while load Pengaturan.fxml file : \n"+ex
            );
            pengaturanView = new BorderPane();
        }
    }
    
    private void showPage(Node page, String title, String imgPath) {
        homeRoot.setCenter(page);
        textTitle.setText(title);
        textTitle.setStyle("-fx-graphic: "+imgPath);
    }
}
