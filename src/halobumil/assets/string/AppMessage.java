/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.assets.string;

/**
 *
 * @author DERI
 */
public class AppMessage {
    
    // Account Usage
    public static String LOGIN_SUCCESS = "Login berhasil.\nSelamat datang kembali, Admin.",
            LOGIN_FAILED = "Login gagal.\nSilahkan cek kembali username dan password anda.",
            REGISTER_SUCCESS = "Berhasil mendaftar.\nSelamat datang di Aplikasi Halo Bumil.",
            REGISTER_FAILED = "Gagal mendaftar, silahkan coba lagi.",
            UPDATE_ACCOUNT_SUCCESS = "Berhasil merubah data akun.",
            ERROR_INACTIVE_ACCOUNT = "Tidak ada akun yang aktif.";
    
    // Pasien, Trimester and Kunjungan Usage
    public static String 
            DELETE_KUNJUNGAN_CONFIRMATION = "Apakah anda yakin akan menghapus data kunjungan tersebut?",
            DELETE_LIST_SMS_CONFIRMATION = "Apakah anda yakin akan menghapus pasien tersebut dari daftar pengiriman SMS?",
            DELETE_PASIEN_CONFIRMATION = "Apakah anda yakin akan menghapus data pasien tersebut?\n"
            + "Semua data kunjungan pasien bersangkutan akan ikut terhapus.";
    public static String
            INSERT_TRIMESTER_SUCCESS = "Berhasil menambah data trimester pasien.",
            DELETE_TRIMESTER_SUCCESS = "Berhasil menghapus data trimester pasien.",
            DELETE_TRIMESTER_CONFIRMATION = "Apakah anda yakin akan menghapus data trimester tersebut?";
    public static String TITLE_DIALOG_SUCCESS = "Sukses",
            TITLE_DIALOG_ERROR = "Terjadi Kesalahan",
            TITLE_DIALOG_INFO = "Informasi",
            TITLE_DIALOG_CONFIRM = "Pesan Konfirmasi";
    public static String 
            ERROR_NOTHING_FIELD_SELECTED = "Silahkan pilih salah satu data di dalam tabel terlebih dahulu.";
    public static String
            ERROR_GENERAL_DATABASE = "Terjadi kesalahan ketika mengakses database.",
            ERROR_EMPTY_RESULT = "Tidak ada data yang ditemukan dalam database.";
}
