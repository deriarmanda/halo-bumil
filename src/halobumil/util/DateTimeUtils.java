/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 *
 * @author DERI
 */
public class DateTimeUtils {
    /** The date pattern that is used for conversion. Change as you wish. */
    private static final String TAG_NAME = DateTimeUtils.class.getSimpleName();
    private static final String DATE_PATTERN = "dd/MM/yyyy";
    private static final String TIME_PATTERN = "HH:mm";
    private static final String HUMAN_PATTERN = "EEEE, d MMMM yyyy";
    private static final String SPLITTER = " ";
    
    /** The date formatter. */
    private static final DateTimeFormatter DATE_FORMATTER = 
            DateTimeFormatter.ofPattern(DATE_PATTERN);
    private static final DateTimeFormatter DATE_TIME_FORMATTER = 
            DateTimeFormatter.ofPattern(DATE_PATTERN + SPLITTER + TIME_PATTERN);
    private static final DateTimeFormatter HUMAN_DATE_FORMATTER = 
            DateTimeFormatter.ofPattern(HUMAN_PATTERN);
    
    /**
     * Returns the given date as a well formatted String. The above defined 
     * {@link DateUtil#DATE_PATTERN} is used.
     * 
     * @param dateTime the dateTime to be returned as a string
     * @return formatted string
     */
    public static String format(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return DATE_TIME_FORMATTER.format(dateTime);
    }
    public static String format(LocalDate dateTime) {
        if (dateTime == null) {
            return null;
        }
        return DATE_FORMATTER.format(dateTime);
    }

    /**
     * Converts a String in the format of the defined {@link DateUtil#DATE_PATTERN} 
     * to a {@link LocalDateTime} object.
     * 
     * Returns null if the String could not be converted.
     * 
     * @param dateTimeString the date as String
     * @return the date object or null if it could not be converted
     */
    public static LocalDateTime parse(String dateTimeString) {
        try {
            return DATE_TIME_FORMATTER.parse(dateTimeString, LocalDateTime::from);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
    public static LocalDate parseDateOnly(String dateString) {
        try {
            return DATE_FORMATTER.parse(dateString, LocalDate::from);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
    
    public static Boolean isMorningNow() {
        String currHour = DateTimeFormatter.ofPattern("H").format(LocalDateTime.now());
        int hour = Integer.parseInt(currHour);
        //System.out.println("isMorningNow: "+hour);
        return hour <= 12;
    }

    /**
     * Checks the String whether it is a valid date.
     * 
     * @param dateTimeString
     * @return true if the String is a valid date
     */
    public static boolean validDate(String dateTimeString) {
        // Try to parse the String.
        return DateTimeUtils.parse(dateTimeString) != null;
    }
    
    public static String getDateOnly(String dateTimeString) {
        return dateTimeString.split(SPLITTER)[0];
    }
    
    public static String getTimeOnly(String dateTimeString) {
        return dateTimeString.split(SPLITTER)[1];
    }
    
    public static String getCurrentHumanReadableDate() {
        return HUMAN_DATE_FORMATTER.format(LocalDate.now());
    }
}
