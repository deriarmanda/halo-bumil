/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import halobumil.assets.string.AppMessage;
import halobumil.config.DatabaseConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author DERI
 */
public class DatabaseHelper {
    
    private static DatabaseHelper sInstance;
    private final Connection dbConn;
    
    private DatabaseHelper() {
        dbConn = DatabaseConfig.getConnection();
    }
    
    public static DatabaseHelper getInstance() { 
        if (sInstance == null) sInstance = new DatabaseHelper();
        else if (sInstance.dbConn == null) sInstance = new DatabaseHelper();
        return sInstance;
    }
    
    public void executeQuery(String query, Object... values) throws SQLException {
        PreparedStatement stmt = dbConn.prepareStatement(query);
        for (int i=1 ;i<=values.length; i++) {     
            if (values[i-1] instanceof Integer) {
                stmt.setInt(i, (int) values[i-1]);
            } else {
                stmt.setString(i, (String) values[i-1]); 
            }
        }
        stmt.executeUpdate();
    }
    
    public ResultSet readData(String query, Object... values) throws SQLException {
        ResultSet rs;
        PreparedStatement stmt = dbConn.prepareStatement(query);
        for (int i=1 ;i<=values.length; i++) {                
            if (values[i-1] instanceof Integer) {
                stmt.setInt(i, (int) values[i-1]);
            } else {
                stmt.setString(i, (String) values[i-1]); 
            }
        }
        rs = stmt.executeQuery();
        return rs;
    }
    
    public int getInsertedIdQuery(String query, Object... values) throws SQLException {
        int generatedKey;
        PreparedStatement stmt = dbConn.prepareStatement(
                query, 
                Statement.RETURN_GENERATED_KEYS
        );
        for (int i=1 ;i<=values.length; i++) {     
            if (values[i-1] instanceof Integer) {
                stmt.setInt(i, (int) values[i-1]);
            } else {
                stmt.setString(i, (String) values[i-1]); 
            }
        }
        int affectedRows = stmt.executeUpdate();
        if (affectedRows == 0) throw new SQLException(AppMessage.ERROR_GENERAL_DATABASE);

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) generatedKey = rs.getInt(1);
        else throw new SQLException(AppMessage.ERROR_EMPTY_RESULT);
        return generatedKey;
    }
}
