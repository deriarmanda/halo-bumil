/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DERI
 */
public class LoggerUtils {
    
    public static void showInfoMessage(String source, String msg) {
        Logger.getLogger(source).log(Level.INFO, msg);
    }
    
    public static void showWarningMessage(String source, String msg) {
        Logger.getLogger(source).log(Level.WARNING, msg);
    }
    
    public static void showErrorMessage(String source, String msg) {
        Logger.getLogger(source).log(Level.SEVERE, msg);
    }
    
}
