/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import halobumil.config.AppConfig;
import halobumil.data.model.SMS;
import halobumil.data.repository.SMSRepository;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.application.Platform;
import org.jsmsengine.CService;

/**
 *
 * @author DERI
 */
@Deprecated
public class SmsSenderTask extends Thread {

    private final String TAG_NAME = SmsSenderTask.class.getSimpleName();
    private final String QUERY_UPDATE_STATUS_SMS = 
            "UPDATE trimester SET status_pesan_kemarin = ?, "
            + "status_pesan_pagi = ?, status_pesan_sore = ? "
            + "WHERE uid = ?;";
    private final String QUERY_GET_TODAY_LIST = 
            "SELECT t.uid, p.nama, p.telepon, t.tanggal, t.status_pesan_kemarin, "
            + "t.status_pesan_pagi, t.status_pesan_sore FROM pasien p "
            + "INNER JOIN trimester t ON p.uid = t.uid_pasien WHERE "
            + "t.tanggal = ? OR t.tanggal = ?;";
    
    private final AppConfig appConfig;
    private final DatabaseHelper dbHelper;
    private final DialogBuilder dialogBuilder;
    private final CService modem;
    private final SMSRepository smsRepo;
    private final ArrayList<SMS> listSms;
    
    public boolean isRunning;
    
    public SmsSenderTask() {
        appConfig = AppConfig.getInstance();
        dbHelper = DatabaseHelper.getInstance();
        dialogBuilder = DialogBuilder.getInstance();
        smsRepo = SMSRepository.getInstance();
        modem = new CService("COM20", 9999, "Huawei", "");
        listSms = new ArrayList<>();
        isRunning = false;
    }
    
    @Override
    public void run() {
        isRunning = true;
        try {
            modem.connect();
            Platform.runLater(() -> {
                dialogBuilder.showInfoDialog(
                        "Berhasil terhubung dengan Modem GSM dengan nama : "
                                +modem.getDeviceInfo().getManufacturer()
                );
            });
        } catch (Exception ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
            Platform.runLater(() -> {
                dialogBuilder.showErrorDialog(
                    "Terjadi kesalahan saat menghubungkan ke Modem GSM.\n"
                            + "Pastikan Modem GSM telah terhubung dan restart aplikasi."
                );
            });
            return;
        }
        do {
            try {
                updateSMSList();
            } catch(SQLException ex) {
                LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                continue;
            }
            
            listSms.stream().map((sms) -> {
                String humanDate = sms.getHumanFormatTanggal().get(),
                        number = sms.getTelepon().get();
                if (humanDate.equals("Besok")) {
                    if (sms.getStatusKemarin().get().equals("Menunggu")) {
                        sendSms(number, smsRepo.getTeksKemarin());   
                        sms.setStatusKemarin("Terkirim");
                    }
                } else {
                    boolean isMorning = DateTimeUtils.isMorningNow();
                    if (isMorning) {
                        if (sms.getStatusPagi().get().equals("Menunggu")) {
                            sendSms(number, smsRepo.getTeksPagi());
                            sms.setStatusKemarin("Terkirim");
                            sms.setStatusPagi("Terkirim");
                        }
                    } else {
                        if (sms.getStatusSore().get().equals("Menunggu")) {
                            sendSms(number, smsRepo.getTeksSore());
                            sms.setStatusKemarin("Terkirim");
                            sms.setStatusPagi("Terkirim");
                            sms.setStatusSore("Terkirim");
                        }
                    }
                }
                return sms;                
            }).forEachOrdered((sms) -> {
                try {
                    updateSmsStatus(sms);
                } catch (SQLException ex) {
                    LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
                }
            });
        } while (appConfig.isRunning());
        isRunning = false;
    }    
    
    private void sendSms(String number, String msg) {
//        COutgoingMessage msgBuilder = new COutgoingMessage(
//                number, msg
//        );
//        msgBuilder.setMessageEncoding(CMessage.MESSAGE_ENCODING_7BIT);
//
//        try {
//            modem.sendMessage(msgBuilder);
//        } catch (Exception ex) {
//            LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
//            dialogBuilder.showErrorDialog(
//                    "Terjadi kesalahan ketika mengirim SMS ke nomor "
//                            +number+".\n("+ex.getMessage()+")."
//            );
//        }
    }
    
    private void updateSMSList() throws SQLException {        
        listSms.clear();
        ResultSet rs = dbHelper.readData(
                QUERY_GET_TODAY_LIST,
                DateTimeUtils.format(LocalDate.now()),
                DateTimeUtils.format(LocalDate.now().plusDays(1))
        );
        while (rs.next()) {
            SMS t = new SMS(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7)
            );
            listSms.add(t);
        }
    }
    
    private void updateSmsStatus(SMS sms) throws SQLException {
        dbHelper.executeQuery(
                QUERY_UPDATE_STATUS_SMS,
                sms.getStatusKemarin().get(),
                sms.getStatusPagi().get(),
                sms.getStatusSore().get(),
                sms.getUid()
        );
    }
    
}
