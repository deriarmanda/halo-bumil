/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author Deri Armanda
 */
public class SmsSenderThread extends Thread {

    private static SmsSenderThread sInstance;
    private ServerSocket server;
    private ArrayList<PrintWriter> writerList;
    private boolean isRunning;
    
    private SmsSenderThread() {
        try {
            server = new ServerSocket(9999);
            writerList = new ArrayList<>();
        } catch (IOException ex) {
            Logger.getLogger(SmsSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static SmsSenderThread getInstance() {
        if (sInstance == null) sInstance = new SmsSenderThread();
        return sInstance;
    }
    
    @Override
    public void run() {
        isRunning = true;
        while(isRunning) {
            try {
                Socket client = server.accept();
                PrintWriter writer = new PrintWriter(client.getOutputStream());
                writerList.add(writer);
                Platform.runLater(() -> {
                    DialogBuilder.getInstance().showInfoDialog("Client "+client.getInetAddress().getHostName()+" terhubung ke aplikasi.");
                });
            } catch (IOException ex) {
                Logger.getLogger(SmsSenderThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            server.close();
        } catch (IOException ex) {
            Logger.getLogger(SmsSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }
    
    public void sendSms(String target, String message) {
        for (PrintWriter writer : writerList) {
            writer.println(target+";"+message);
            writer.flush();
        }
    }
}
