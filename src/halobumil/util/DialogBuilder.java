/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util;

import halobumil.assets.string.AppMessage;
import halobumil.config.AppConfig;
import halobumil.util.dialog.MessageDialogController;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author DERI
 * Example initialize: DialogBuilder.init(this extends? Stage);
 * Example usage: DialogBuilder.getInstance().showSuccessDialog("message");
 * 
 */
public class DialogBuilder {
    
    private final String TAG_NAME = DialogBuilder.class.getSimpleName();
    private final Stage MAIN_STAGE;
    private final Image 
            IC_MSG_CONFIRM = new Image("halobumil/assets/image/ic_dialog_confirm_96px.png"),
            IC_MSG_SUCCESS = new Image("halobumil/assets/image/ic_dialog_success_96px.png"),
            IC_MSG_ERROR = new Image("halobumil/assets/image/ic_dialog_error_96px.png"),
            IC_MSG_INFO = new Image("halobumil/assets/image/ic_dialog_info_96px.png");
    
    private static DialogBuilder sInstance;
    
    private AppConfig appConfig;
    private Stage msgDialogStage;
    private MessageDialogController msgController;
    
    private DialogBuilder(Stage mainStage) {
        MAIN_STAGE = mainStage;
        appConfig = AppConfig.getInstance();
        
        initMessageDialog();
    }
    
    public static void init(Stage mainStage) {
        sInstance = new DialogBuilder(mainStage);
    }
    
    public static DialogBuilder getInstance() {
        return sInstance;
    }
    
    public void showConfirmDialog(
            String msg, 
            Callback callback
    ) {
        msgController.setType(MessageDialogController.TYPE_CONFIRM);
        msgController.setCallback(callback);
        msgController.setContents(
                IC_MSG_CONFIRM, 
                AppMessage.TITLE_DIALOG_CONFIRM, 
                msg
        );
        msgDialogStage.setTitle(AppMessage.TITLE_DIALOG_CONFIRM);
        msgDialogStage.showAndWait();
    }
    
    public void showSuccessDialog(String msg) {
        msgController.setType(MessageDialogController.TYPE_MESSAGE);
        msgController.setContents(IC_MSG_SUCCESS, AppMessage.TITLE_DIALOG_SUCCESS, msg);
        msgDialogStage.setTitle(AppMessage.TITLE_DIALOG_SUCCESS);
        msgDialogStage.showAndWait();
    }
    
    public void showInfoDialog(String msg) {
        msgController.setType(MessageDialogController.TYPE_MESSAGE);
        msgController.setContents(IC_MSG_INFO, AppMessage.TITLE_DIALOG_INFO, msg);
        msgDialogStage.setTitle(AppMessage.TITLE_DIALOG_INFO);
        msgDialogStage.showAndWait();
    }
        
    public void showErrorDialog(String msg) {
        msgController.setType(MessageDialogController.TYPE_MESSAGE);
        msgController.setContents(IC_MSG_ERROR, AppMessage.TITLE_DIALOG_ERROR, msg);
        msgDialogStage.setTitle(AppMessage.TITLE_DIALOG_ERROR);
        msgDialogStage.showAndWait();
    }
        
    private void initMessageDialog() {
        try {
            FXMLLoader loader = new FXMLLoader(DialogBuilder.class.getResource("/halobumil/util/dialog/MessageDialog.fxml"));
            Parent content = loader.load();
            
            msgDialogStage = new Stage();
            msgDialogStage.initModality(Modality.WINDOW_MODAL);
            msgDialogStage.initOwner(MAIN_STAGE);
            msgDialogStage.setScene(new Scene(content));
            msgDialogStage.getIcons().add(appConfig.APP_ICON);
            
            msgController = (MessageDialogController) loader.getController();
            msgController.setDialogStage(msgDialogStage);
        } catch (IOException ex) {
            LoggerUtils.showErrorMessage(TAG_NAME, ex.getMessage());
        }
    }
    
    public interface Callback {
        void onConfirmationApproved();
    }
}
