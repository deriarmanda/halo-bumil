/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package halobumil.util.dialog;

import com.jfoenix.controls.JFXButton;
import halobumil.util.DialogBuilder;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author DERI
 */
public class MessageDialogController implements Initializable {

    public static final int TYPE_MESSAGE = 310;
    public static final int TYPE_CONFIRM = 311;
    
    @FXML private ImageView icMessage;
    @FXML private Label textTitle;
    @FXML private Label textMessage;
    @FXML private JFXButton buttonClose;
    @FXML private JFXButton buttonYa;
    @FXML private JFXButton buttonTidak;
    
    private Stage dialogStage;
    private DialogBuilder.Callback callback;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setCallback(DialogBuilder.Callback callback) {
        this.callback = callback;
    }
    
    public void setType(int type) {
        switch(type) {
            case TYPE_MESSAGE: 
                buttonClose.setVisible(true);
                buttonYa.setVisible(false);
                buttonTidak.setVisible(false);
                break;
            case TYPE_CONFIRM: 
                buttonClose.setVisible(false);
                buttonYa.setVisible(true);
                buttonTidak.setVisible(true);
                break;
            default: 
                break;
        }
    }
    
    public void setContents(Image img, String title, String msg) {
        icMessage.setImage(img);
        textTitle.setText(title);
        textMessage.setText(msg);
    }
    
    @FXML private void closeDialog() {
        dialogStage.close();
    }
    @FXML private void onYesButtonClick() {
        if (callback != null) callback.onConfirmationApproved();
        dialogStage.close();
    }
}
